<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use common\models\Page;
/* @var $this yii\web\View */
/* @var $searchModel common\models\WorkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$pages_mod = Page::find()->all();
$pages = ArrayHelper::map($pages_mod,'id','title');
$this->title = 'Works';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Work', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            [
                'attribute' => 'photo',
                'format' => 'html',
                'filter' => false,
                'value' => function ($data) {
                    return Html::img(Yii::$app->imagemanager->getImagePath($data->img, 200, 200,'inset'));
                },
            ],
            [
            'attribute'=>'service_id',
            'filter'=>$pages,
            'value' => function ($data) {
                return $data->service ? $data->service->name : '';
            },
        ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
