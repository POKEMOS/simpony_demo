<?php

use yii\db\Migration;

/**
 * Handles adding page to table `price`.
 * Has foreign keys to the tables:
 *
 * - `price_page`
 */
class m170619_144013_add_page_column_to_price_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('price', 'page', $this->integer());

        // creates index for column `page`
        $this->createIndex(
            'idx-price-page',
            'price',
            'page'
        );

        // add foreign key for table `price_page`
        $this->addForeignKey(
            'fk-price-page',
            'price',
            'page',
            'price_page',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `price_page`
        $this->dropForeignKey(
            'fk-price-page',
            'price'
        );

        // drops index for column `page`
        $this->dropIndex(
            'idx-price-page',
            'price'
        );

        $this->dropColumn('price', 'page');
    }
}
