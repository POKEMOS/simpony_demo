var inst = $('[data-remodal-id=modal]').remodal();

//Галерия//////////////////////////////////////
$(function() {
    $('.tl').glisse({
        changeSpeed: 550,
        speed: 500,
        effect: 'roll',
        fullscreen: true
    });
});

/*$(function() {
    $('ul.tabs_gallery').on('click', 'li:not(.active)', function() {
        $(this)
            .addClass('active').parent().siblings().find('li').removeClass('active')
            .closest('div.tabs').find('div.tabs_portfolio').removeClass('active').eq($(this).parent().index()).addClass('active');
    });

});*/

$(function () {




    $('.forest').removeClass('active');
    $('.tabs_portfolio.active').hide();
    $('.forest').click(function () {
        var num = $(this).attr('data-num');
        $('.tabs_portfolio[data-num="'+num+'"]').addClass('active');
        $('.tabs_portfolio.active').show();
        $('body').css('overflow', 'hidden');
    });
    $(".close").click(function () {        
        $('body').css('overflow', 'auto');
        $(".tabs_portfolio").hide();
        $(".tabs_portfolio").removeClass('active');
        $('.forest').removeClass('active');
    });
});


///Внутриняя галерию в Галериии///
$(function() {
    $('ul.tabs__caption').on('click', 'li:not(.axote)', function() {
        $(this)
            .addClass('axote').siblings().removeClass('axote')
            .closest('div.tabs_portfolio.active').find('.galleryImg').hide();
        $(this).closest('div.tabs_portfolio.active').find('.galleryImg[data-complete="' + $(this).data('complete') + '"]').show();
    });

});
$(".forest.active").on("click", function() {
    $('.tabs_portfolio.active').toggleClass();
});
$(".tabs_portfolio.active").on("click", function() {
    $('.tabs_portfolio.active').hide();
});
//////////////////////////////////////////////

/*ymaps.ready(function () {
    var myMap = new ymaps.Map('map', {
            center: [55.811265, 37.520950],
            zoom: 16
        }, {
            searchControlProvider: 'yandex#search'
        }),

        // Создаём макет содержимого.
        MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
            '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
        ),

        myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
            hintContent: 'Москва ул. Часовая 28',
            balloonContent: 'ООО "СтройФорс 57"'
        }, {
            // Опции.
            // Необходимо указать данный тип макета.
            iconLayout: 'default#image',
            // Своё изображение иконки метки.
            iconImageHref: 'img/geotag.png',
            // Размеры метки.
            iconImageSize: [30, 42],
            // Смещение левого верхнего угла иконки относительно
        })


    myMap.geoObjects
        .add(myPlacemark)
        .add(myPlacemarkWithContent);
});*/

$('#sl_1_otdelka, #sl_price, #sl_2_price').owlCarousel({
    //autoplay: 5000,

    nav: false,
    navText: false,
    loop: true,
    rewind: false,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
            nav: true
        },
        600: {
            items: 2,
            nav: false
        },
        1000: {
            items: 3,
            nav: true,
            loop: false
        }
    }
});


$('#sl_one').owlCarousel({
    autoplay: 5000,

    nav: false,
    navText: false,
    loop: true,
    rewind: false,
    responsiveClass: false,
    items: 1,
});
$('#sl_tabs').owlCarousel({
    //autoplay: 5000,

    nav: false,
    navText: false,
    loop: true,
    rewind: false,
    responsiveClass: false,
    items: 1,
});
$('#sl_tabs2').owlCarousel({
    //autoplay: 5000,

    nav: false,
    navText: false,
    loop: true,
    rewind: true,
    responsiveClass: true,
    items: 1,
});

$('#sl_job').owlCarousel({
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    autoplay: true,
    nav: false,
    navText: false,
    loop: true,
    rewind: true,
    responsiveClass: false,
    items: 2,
    responsive: {
        0: {
            items: 1,
            nav: true
        },
        600: {
            items: 1,
            nav: false
        },
        1000: {
            items: 2,
            nav: true,
            loop: false
        }
    }
});
$('#sl_dock, #sl_classic, #sl_price').owlCarousel({
    //autoplay: 5000,

    nav: false,
    navText: false,
    loop: true,
    rewind: false,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
            nav: true
        },
        600: {
            items: 2,
            nav: false
        },
        1000: {
            items: 3,
            nav: true,
            loop: false
        }
    }
});
$('#sl_otziv').owlCarousel({
    //autoplay: 5000,
    dots: true,
    nav: false,
    navText: false,
    loop: true,
    rewind: false,
    responsiveClass: false,
    items: 1,
});
$('ul.tabs_part').on('click', 'li:not(.active)',
    function() {
        $(this)
            .addClass('active').siblings().removeClass('active')
            .closest('div.tabs').find('div.carusel')
            .removeClass('active').eq($(this).index()).addClass('active');
    });
(function() {
    var boxes = [],
        els, i, l;
    if (document.querySelectorAll) {
        els = document.querySelectorAll('a[rel=simplebox]');
        Box.getStyles('simplebox_css', '/styles/simplebox.css');
        Box.getScripts('simplebox_js', '/js/simplebox.js', function() {
            simplebox.init();
            for (i = 0, l = els.length; i < l; ++i)
                simplebox.start(els[i]);
            simplebox.start('a[rel=simplebox_group]');
        });
    }
})();
$(window).scroll(function() {
    $('.animca').each(function() {
        var imgAnim = $(this).offset().top;
        var topOfWindow = $(window).scrollTop();
        if (imgAnim < topOfWindow + 500) {
            $(this).removeClass('nova');
            $(this).addClass('zoomIn');

        }
    });
    $('.anim_plus').each(function() {
        var imgAnim = $(this).offset().top;
        var topOfWindow = $(window).scrollTop();
        if (imgAnim < topOfWindow + 700) {
            $(this).addClass('fadeInDownBig');
        }
    });
});

$(function() {
    var num = $(".namber");
    num.each(function(indx, el) {
        var max = $(el).data("max");
        var duration = 5000;
        var visibility = checkViewport(el);
        $(el).on("animeNum", function() {
            $({
                n: 0
            }).animate({
                n: max
            }, {
                easing: "linear",
                duration: duration,
                step: function(now, fx) {
                    $(el).html(now | 0)
                }
            })
        }).data("visibility", visibility);
        visibility && $(el).trigger("animeNum")
    });

    function checkViewport(el) {
        var H = document.documentElement.clientHeight,
            h = el.scrollHeight,
            pos = el.getBoundingClientRect();
        return pos.top + h > 0 && pos.bottom - h < H
    }
    $(window).scroll(function() {
        num.each(function(indx, el) {
            var visibility = checkViewport(el);
            el = $(el);
            var old = el.data("visibility");
            old != visibility && el.data("visibility", visibility) && !old && el.trigger("animeNum")
        })
    })

    $(function() {
        if ($('#chose_file').length) {
            $('#chose_file').click(function() {
                $('#chose_file_input').click();
                return (false);
            });

            $('#chose_file_input').change(function() {
                $('#chose_file_text').html($(this).val());
            }).change(); // .change() в конце для того чтобы событие сработало при обновлении страницы
        }
    });


    $(function() {
        $('form').submit(function() {
            var isOk = true;
            $('input[type=file][max-size]').each(function() {
                if (typeof this.files[0] !== 'undefined') {
                    var maxSize = parseInt($(this).attr('max-size'), 10),
                        size = this.files[0].fileSize;
                    isOk = maxSize > size;
                    return isOk;
                }
            });
            return isOk;
        });
        var size = 0;
        $(document).delegate('#chose_file_input', 'change', function(e) {
            size = $(this).prop('files')[0].size / 1000000;
        })

        $(document).delegate('.form-calc-c', 'submit', function(e) {
            if (size > 5) {
                $(this).closest('#secondForm').find('.msgs').text('Размер загружаемого файла не должен превышать 5мб!');
                e.preventDefault();
            }
        })
    });
});

<!-- Yandex.Metrika counter -->
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter44951161 = new Ya.Metrika({
                id:44951161,
                clickmap:true,
                trackLinks:true,
                accurateTrackBounce:true,
                webvisor:true,
                trackHash:true
            });
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = "https://mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
<!-- /Yandex.Metrika counter -->
