<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "contacts".
 *
 * @property integer $id
 * @property string $phone
 * @property string $email
 * @property string $addr
 */
class Contacts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contacts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone', 'email', 'addr','year_number','work_number','clients_number','projects_number','callac'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phone' => 'Phone',
            'email' => 'Email',
            'addr' => 'Addr',
            'callac' => 'Приём звонков',
            'year_number' => 'Лет на рынке',
            'work_number' => 'Выполнено работ',
            'clients_number' => 'Довольных клиентов %',
            'projects_number' => 'Проекты на стадии сдачи',
        ];
    }
}
