<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PriceBlocks */

$this->title = 'Create Price Blocks';
$this->params['breadcrumbs'][] = ['label' => 'Price Blocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-blocks-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
