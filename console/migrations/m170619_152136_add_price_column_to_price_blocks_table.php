<?php

use yii\db\Migration;

/**
 * Handles adding price to table `price_blocks`.
 * Has foreign keys to the tables:
 *
 * - `price`
 */
class m170619_152136_add_price_column_to_price_blocks_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('price_blocks', 'price', $this->integer());

        // creates index for column `price`
        $this->createIndex(
            'idx-price_blocks-price',
            'price_blocks',
            'price'
        );

        // add foreign key for table `price`
        $this->addForeignKey(
            'fk-price_blocks-price',
            'price_blocks',
            'price',
            'price',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `price`
        $this->dropForeignKey(
            'fk-price_blocks-price',
            'price_blocks'
        );

        // drops index for column `price`
        $this->dropIndex(
            'idx-price_blocks-price',
            'price_blocks'
        );

        $this->dropColumn('price_blocks', 'price');
    }
}
