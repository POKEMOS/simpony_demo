<?php

use yii\db\Migration;

/**
 * Handles adding us to table `work`.
 * Has foreign keys to the tables:
 *
 * - `page`
 */
class m170605_194103_add_us_column_to_work_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('work', 'service_id', $this->integer());

        // creates index for column `service_id`
        $this->createIndex(
            'idx-work-service_id',
            'work',
            'service_id'
        );

        // add foreign key for table `page`
        $this->addForeignKey(
            'fk-work-service_id',
            'work',
            'service_id',
            'page',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `page`
        $this->dropForeignKey(
            'fk-work-service_id',
            'work'
        );

        // drops index for column `service_id`
        $this->dropIndex(
            'idx-work-service_id',
            'work'
        );

        $this->dropColumn('work', 'service_id');
    }
}
