<section class="m_box_carusel_text">
    <div class="middle">
        <h3>ОТЗЫВЫ</h3>
        <div class="owl-carousel owl-theme" id="sl_otziv">
            <?foreach($comments as $comment):?>
                <div class="item">
                    <?if($comment->video):?>
                        <video width="200" controls style="float:left;">
                            <source src="/img/video/<?=$comment->video?>" type="video/mp4">
                        </video>
                    <?else:?>
                        <img src="<?=Yii::$app->imagemanager->getImagePath($comment->photo, 200, 200,'inset')?>" alt="">
                    <?endif;?>
                    <div class="ramka">
                        <div><?=$comment->name?>, <?=$comment->city?> <span><?=$comment->date?></span></div>
                        <p><?=$comment->description?></p>

                    </div>
                </div>
            <?endforeach;?>
        </div>
    </div>
</section>