<section class="sf_top_text">
    <div class="middle">
        <h1>О нас</h1>
        <?=yii::$app->controller->renderPartial('//widgets/footer_callback')?>
        <div class="text_top-ramka">
            <div>
                <div class="sf_contact_ico">
                    <div class="flex_mid">
                        <div style="width: 100%;">
                            
                            <div style="display:block;">
                                <p>
                                    <?foreach($contacts as $contact):?>
                                        <?=$contact->phone?><br>
                                    <?endforeach;?>
                                </p>
                                <p>Прием звонков:
                                    <br>
                                    <?foreach($contacts as $contact):?>
                                        <?if($contact->callac):?>
                                            <?=$contact->callac?><br>
                                        <?endif;?>
                                    <?endforeach;?>
                                </p>
                                <p>
                                    <?foreach($contacts as $contact):?>
                                        <?=$contact->addr?><br>
                                    <?endforeach;?>
                                </p>
                                <p>
                                    <?foreach($contacts as $contact):?>
                                        <?=$contact->email?><br>
                                    <?endforeach;?>
                                </p>
                                
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>

        </div>
        <div class="ico_soc">
            <img src="/img/Odnoklassniki.png" alt="">
            <img src="/img/facebook.png" alt="">
            <img src="/img/instagram.png" alt="">
            <img src="/img/VKontakte.png" alt="">
            <img src="/img/twitter.png" alt="">
        </div>
    </div>
</section>