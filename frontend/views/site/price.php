<section class="sf_top_text akk selle">
    <div class="middle">
        <p>Стоимость</p>
        <h1><strong>ГОТОВЫЙ ДОМ</strong> ОТ 999 000 Р</h1>

        <div class="m_b_cons_call right">
            <form action="/callback" class="flex_col" method="post">
                <div>
                    <input placeholder="Имя" name="name" type="text" required>
                </div>
                <div>
                    <input placeholder="Телефон" name="phone" type="text" required>
                </div>
                <div>
                    <input type="submit" onclick="yaCounter44951161.reachGoal('Know_More'); return true;" value="ХОЧУ УЗНАТЬ ПОДРОБНЕЕ">
                </div>
            </form>
        </div>

        <div class="tabs_price-views">

            <ul class="tabs_list-uslugi">
                <?foreach($price_pages as $key => $page):?>
                    <li class="<?=$key == 0 ? 'active' : ''?>"><?=$page->name?></li>
                <?endforeach;?>
            </ul>

            <?foreach($price_pages as $key => $page):?>
                <div class="tabs_price_right_box <?=$key == 0 ? 'active' : ''?>">
                <div class="other_remont_price_viewer">
                    <div>
                        <p class="left">
                            <?=$page->name?>
                        </p>
                        <p class="right">
                            м<b>2</b>
                        </p>
                    </div>
                    <?foreach($page->prices as $price):?>
                        <div>
                            <p class="left">
                                <?=$price->name?>
                            </p>
                            <p class="right">
                                <span><?=$price->old_price ? $price->old_price.'р' : ''?></span>
                                <?=$price->price?>
                            </p>
                        </div>
                    <?endforeach;?>
                </div>

                <div class="price_info-list">
                    <?foreach($page->blocks as $block):?>
                    <div class="list_box">
                        <img src="<?=Yii::$app->imagemanager->getImagePath($block->img, 300, 300,'inset')?>" alt="">
                        <div><?=$block->description?> </div>
                    </div>
                    <?endforeach;?>
                </div>

            </div>
            <?endforeach;?>

        </div>

    </div>
</section>