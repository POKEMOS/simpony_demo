<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use common\models\Price;

$price_all = Price::find()->where(['=','type',0])->all();
$price_floor = Price::find()->where(['=','type',1])->all();
$price_wall = Price::find()->where(['=','type',2])->all();

AppAsset::register($this);
?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>

    <body>
    <?php $this->beginBody() ?>
    <header>
        <div class="middle">
            <div class="logo_img mobi_mid">
                <a href="/"><img src="/img/logo.png"></a>
            </div>
            <a class="toggle-nav" href="#">&#9776;</a>
            <div id="index_top">
                <ul class="navi">
                    <li class="gray">
                        <a href="/about">О нас</a>
                    </li>
                    <li class="gray">
                        <a href="/service">Услуги</a>
                        <ul class="flex_col">
                            <li class="gray">
                                <a href="/service/1">СТРОИТЕЛЬСТВО ДОМОВ ПОД КЛЮЧ</a>
                            </li>
                            <li class="gray">
                                <a href="/service/2">Отделочные работы</a>
                            </li>
                            <li class="gray">
                                <a href="/service/3">Монтаж вентилируемых фасадов</a>
                            </li>
                            <li class="gray">
                                <a href="/service/4">Проектирование и дизайн</a>
                            </li>
                            <li class="gray">
                                <a href="/service/5">Архитектурное освещение и реклама</a>
                            </li>
                            <li class="gray">
                                <a href="/service/6">Изготовление фасадных кассет</a>
                            </li>
                            <li class="gray">
                                <a href="/service/8">Кровельные работы</a>
                            </li>
                            <li class="gray">
                                <a href="/service/9">Промышленные и наливные полы</a>
                            </li>
                        </ul>
                    </li>
                    <li class="gray">
                        <a href="/price">Стоимость</a>
                    </li>
                    <li class="gray">
                        <a href="/portfolio">Портфолио</a>
                    </li>
                    <li class="gray">
                        <a href="/sales">Акции</a>
                    </li>
                    <li class="gray">
                        <a href="/contacts">Контакты</a>
                    </li>
					<li class="gray">
                        <a href="/index#otziv">Отзывы</a>
                    </li> 
                </ul>
            </div>
            <a class="call-modall" data-remodal-target="secondModal2" href="#"><img src="/img/789.png"></a>
        </div>
    </header>
    <main>
        <?=$content?>
        <section class="m_box_maps">


            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2241.962323109254!2d37.51851721598496!3d55.81125448056904!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b5483336d1518f%3A0xa907b855588f9477!2z0YPQuy4g0KfQsNGB0L7QstCw0Y8sIDI4LCDQnNC-0YHQutCy0LAsIDEyNTMxNQ!5e0!3m2!1sru!2sru!4v1497276030797" height="420" frameborder="0" style="border:0" allowfullscreen></iframe>
        </section>
    </main>
    <footer>
        <div class="middle">
            <div>
                <p class="color">Стройфорс — это:</p>
                <p>Ультрасовременные технологии, новейшие материалы.</p>
                <p>Первоклассные специалисты, в том числе имеющие опыт за границей.</p>
                <p>Надежность, компетентность, индивидуальный подход к каждому клиенту</p>
            </div>
            <div>
                <p class="color-min">Адрес: <br>
                    <span>143402, Москва ул. Часовая 28</span><br>
                    <span>4 этаж, офис 11</span>
                </p>
                <p class="color-min">Телефон: <br>
                    <span><a href="tel:+7 (909)649-649-0">+7 (909)649-649-0</a></span>
                </p>
                <p class="imgsoc">
                <a href="https://vk.com/stroyforse57" target="_blank"><img src="/img/VKontakte-min.png" alt=""></a>
                <a href="" target="_blank"><img src="/img/Odnoklassniki-min.png" alt=""></a>
                <a target="_blank" href="https://www.facebook.com/groups/1852936924991705/?ref=aymt_homepage_panel"><img src="/img/facebook-min.png" alt=""></a>
                <a target="_blank" href="https://twitter.com/StroyForse57"><img src="/img/twitter-min.png" alt=""></a>
                <a target="_blank" href="https://www.instagram.com/stroyforse57/"><img src="/img/instagram-min.png" alt=""></a>
                </p>
            </div>
            <?=yii::$app->controller->renderPartial('//widgets/footer_callback')?>
        </div>
        <div class="copy">
        <a href="http://portfolio-infoinstant.ru/" target="_blank" style="
        ">Сделано в Infoinstant</a> 2017г
        </div>
    </footer>


    <!--Модальное окно-->
    <div class="remodal-wrapper remodal-is-closed" style="display: none;" class="form-send">
        <div class="remodal remodal-is-initialized remodal-is-closed" data-remodal-id="secondModal" data-remodal-options="hashTracking: false,closeOnConfirm: false" tabindex="-1">

            <div class="formArea">

                <form id="secondForm" class="form js-send-form form-send" method="post" action="/mail.php" autocomplete="off">

                    <p class="msgs"></p>
                    <div class="midd_text">
                        1011 белый
                    </div>
                    <img src="/img/1011.png" alt="Цвет">
                    <div class="part_non">
                        <span>Размеры плит постформинг:</span>
                        <ul>
                            <li>Толщина 26 мм; 38 мм</li>
                            <li>Длина 3050 мм</li>
                            <li>Ширина от 600 до 1200 мм</li>
                        </ul>
                        <div class="m_box_line"></div>
                    </div>

                    <div class="midd_text_min">
                        Сделать заказ
                    </div>
                    <input type="hidden" name="type" value="other">
                    <fieldset class="form-fieldset ui-input __first">
                        <input required type="text" tabindex="0" placeholder="Ваше имя" name="username">
                    </fieldset>
                    <fieldset class="form-fieldset ui-input __first">
                        <input type="text" tabindex="0" placeholder="Ваш email" name="email">
                    </fieldset>
                    <fieldset class="form-fieldset ui-input __second">
                        <input required type="tel" id="phone" tabindex="0" placeholder="Ваш телефон" name="phone">
                        <input required type="text" tabindex="0" placeholder="Ваш комментарий" name="phone">
                    </fieldset>

                    <input name="formInfo" class="formInfo" type="hidden" value="ОТПРАВИТЬ">

                    <div class="form-footer">
                        <input type="submit" class="formBtn" onclick="yaCounter44951161.reachGoal('Call_Order'); return true;" value="ОТПРАВИТЬ">
                    </div>

                </form>
            </div>
        </div>
    </div>
    <!--Модальное окно-->

    <?=yii::$app->controller->renderPartial('//widgets/callback')?>

    <!--Модальное окно Монтаж навесного вентИлируемого фасада-->
    <div class="remodal-wrapper remodal-is-closed" style="display: none;" class="form-send">
        <div class="remodal remodal-is-initialized remodal-is-closed" data-remodal-id="secondModalm" data-remodal-options="hashTracking: false,closeOnConfirm: false" tabindex="-1">

            <div class="formArea">

                <form id="secondForm" class="form js-send-form form-send" method="post" action="/mail.php" autocomplete="off">

                    <p class="msgs"></p>
                    <div>
                        <p class="left">
                            Монтаж навесного вентИлируемого фасада
                        </p>
                        <p class="right">
                            м<b>2</b>
                        </p>
                    </div>

                    <?foreach($price_all as $price):?>
                        <div>
                            <p class="left">
                                <?=$price->name?>
                            </p>
                            <p class="right">
							<span><?=$price->old_price ? $price->old_price.'р' : ''?></span>
                                <?=$price->price?>
                            </p>
                        </div>
                    <?endforeach;?>

                </form>
            </div>
        </div>
    </div>
    <!--Модальное окно Монтаж навесного вентИлируемого фасада-->
    <!--Модальное окно ОТДЕЛКА ПОЛОВ-->
    <div class="remodal-wrapper remodal-is-closed" style="display: none;" class="form-send">
        <div class="remodal remodal-is-initialized remodal-is-closed" data-remodal-id="secondModalOtdelka" data-remodal-options="hashTracking: false,closeOnConfirm: false" tabindex="-1">

            <div class="formArea">

                <form id="secondForm" class="form js-send-form form-send" method="post" action="/mail.php" autocomplete="off">

                    <p class="msgs"></p>
                    <div>
                        <p class="left">
                            ОТДЕЛКА ПОЛОВ
                        </p>
                        <p class="right">
                            м<b>2</b>
                        </p>
                    </div>
                    <?foreach($price_floor as $price):?>
                        <div>
                            <p class="left">
                                <?=$price->name?>
                            </p>
                            <p class="right">
							<span><?=$price->old_price ? $price->old_price.'р' : ''?></span>
                                <?=$price->price?>
                            </p>
                        </div>
                    <?endforeach;?>
                </form>
            </div>
        </div>
    </div>
    <!--Модальное окно ОТДЕЛКА ПОЛОВ-->
    <!--Модальное окно ОТДЕЛКА СТЕН-->
    <div class="remodal-wrapper remodal-is-closed" style="display: none;" class="form-send">
        <div class="remodal remodal-is-initialized remodal-is-closed" data-remodal-id="secondModalOtdelkasten" data-remodal-options="hashTracking: false,closeOnConfirm: false" tabindex="-1">

            <div class="formArea">

                <form id="secondForm" class="form js-send-form form-send" method="post" action="/mail.php" autocomplete="off">

                    <p class="msgs"></p>
                    <div>
                        <p class="left">
                            ОТДЕЛКА СТЕН
                        </p>
                        <p class="right">
                            м<b>2</b>
                        </p>
                    </div>
                    <?foreach($price_wall as $price):?>
                        <div>
                            <p class="left">
                                <?=$price->name?>
                            </p>
                            <p class="right">
							<span><?=$price->old_price ? $price->old_price.'р' : ''?></span>
                                <?=$price->price?>
                            </p>
                        </div>
                    <?endforeach;?>
                </form>
            </div>
        </div>
    </div>
    <!--Модальное окно ОТДЕЛКА СТЕН-->
    <!--Модальное окно-->
    <div class="remodal-wrapper remodal-is-closed" style="display: none;" class="form-send">
        <div class="remodal remodal-is-initialized remodal-is-closed" data-remodal-id="secondModal2" data-remodal-options="hashTracking: false,closeOnConfirm: false" tabindex="-1">

            <div class="formArea">

                <form id="secondForm" class="form js-send-form form-send" method="post" action="/mail.php" autocomplete="off">

                    <p class="msgs"></p>
                    <div class="midd_text">
                        Обратный звонок
                    </div>
                    <img src="/img/logo.png" alt="Цвет">

                    <input type="hidden" name="type" value="other">
                    <fieldset style="width: 100%;" class="form-fieldset ui-input __first">
                        <input required type="text" tabindex="0" placeholder="Ваше имя" name="username">
                    </fieldset>
                    <fieldset style="width: 100%;" class="form-fieldset ui-input __first">
                        <input type="text" tabindex="0" placeholder="Ваш email" name="email">
                    </fieldset>
                    <fieldset style="width: 100%;" class="form-fieldset ui-input __second">
                        <input required type="tel" id="phone" tabindex="0" placeholder="Ваш телефон" name="phone">
                        <input required type="text" tabindex="0" placeholder="Ваш комментарий" name="phone">
                    </fieldset>

                    <input name="formInfo" class="formInfo" type="hidden" value="ОТПРАВИТЬ">

                    <div style="width: 100%;" class="form-footer">
                        <input type="submit" class="formBtn" onclick="yaCounter44951161.reachGoal('Call_Order'); return true;" value="Заказать звонок">
                    </div>

                </form>
            </div>
        </div>
    </div>
    <!--Модальное окно-->

    <?=yii::$app->controller->renderPartial('//widgets/callback_calc')?>

    <?php $this->endBody() ?>
    </body>

    <script src="https://code.jquery.com/jquery-migrate-1.4.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


    </html>
<?php $this->endPage() ?>