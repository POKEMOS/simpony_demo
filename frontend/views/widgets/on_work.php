<?
use common\models\Contacts;

$year_number = Contacts::find()->where(['not',['year_number' => null]])->one();
$work_number = Contacts::find()->where(['not',['work_number' => null]])->one();
$clients_number = Contacts::find()->where(['not',['clients_number' => null]])->one();
$projects_number = Contacts::find()->where(['not',['projects_number' => null]])->one();
?>
<section class="m_box_advantage">
    <div class="middle">
        <div class="flex_mid">
            <div><img src="/img/sf_box_pic1.png" alt="pictures">
                <p class="big"><?=$year_number->year_number?></p>
                <p>Лет мы на рынке
                    строительных услуг</p>
            </div>
            <div><img src="/img/sf_box_pic2.png" alt="pictures">
                <p class="big"><?=$year_number->work_number?></p>
                <p>Выполненных
                    работ</p>
            </div>
            <div><img src="/img/sf_box_pic3.png" alt="pictures">
                <p class="big"><?=$year_number->clients_number?>%</p>
                <p>Довольных
                    клиентов</p>
            </div>
            <div><img src="/img/sf_box_pic4.png" alt="pictures">
                <p class="big"><?=$year_number->projects_number?></p>

                <p>Проекта на
                    стадии сдачи</p>
            </div>
        </div>
    </div>
</section>