<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property string $name
 * @property string $title
 * @property string $description_long
 * @property string $description_page
 * @property string $description_sales
 * @property integer $photo
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description_long', 'description_page', 'description_sales'], 'string'],
            [['photo'], 'integer'],
            [['name', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Страница',
            'title' => 'Название на странице стоимости',
            'description_long' => 'Длинное описание на странице услуги',
            'description_page' => 'Описание для блока на странице услуги',
            'description_sales' => 'Описание на странице стоимости',
            'photo' => 'Фотография',
        ];
    }
}
