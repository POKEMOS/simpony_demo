<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PriceBlocksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Price Blocks';
$this->params['breadcrumbs'][] = $this->title;
?>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <ul class="nav navbar-nav">
            <li><a href="/admin/price">Прайс</a></li>
            <li><a href="/admin/pricepage">Страницы</a></li>
        </ul>
    </div>
</nav>
<div class="price-blocks-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Price Blocks', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'photo',
                'format' => 'html',
                'value' => function ($data) {
                    return Html::img(Yii::$app->imagemanager->getImagePath($data->img, 200, 200,'inset'));
                },
            ],
            [
                'attribute' => 'photo',
                'value' => function ($data) {
                    return $data->page->name;
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
