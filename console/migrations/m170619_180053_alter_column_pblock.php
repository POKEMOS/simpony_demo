<?php

use yii\db\Migration;

class m170619_180053_alter_column_pblock extends Migration
{
    public function up()
    {
        // drops foreign key for table `price`
        $this->dropForeignKey(
            'fk-price_blocks-price',
            'price_blocks'
        );

        // drops index for column `price`
        $this->dropIndex(
            'idx-price_blocks-price',
            'price_blocks'
        );
        // creates index for column `price`
        $this->createIndex(
            'idx-price_blocks-price',
            'price_blocks',
            'price'
        );

        // add foreign key for table `price`
        $this->addForeignKey(
            'fk-price_blocks-price',
            'price_blocks',
            'price',
            'price_page',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        // drops foreign key for table `price`
        $this->dropForeignKey(
            'fk-price_blocks-price',
            'price_blocks'
        );

        // drops index for column `price`
        $this->dropIndex(
            'idx-price_blocks-price',
            'price_blocks'
        );

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
