<?php

use yii\db\Migration;

/**
 * Handles adding album to table `city`.
 * Has foreign keys to the tables:
 *
 * - `album`
 */
class m170530_180313_add_album_column_to_city_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('city', 'album_id', $this->integer());

        // creates index for column `album_id`
        $this->createIndex(
            'idx-city-album_id',
            'city',
            'album_id'
        );

        // add foreign key for table `album`
        $this->addForeignKey(
            'fk-city-album_id',
            'city',
            'album_id',
            'album',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `album`
        $this->dropForeignKey(
            'fk-city-album_id',
            'city'
        );

        // drops index for column `album_id`
        $this->dropIndex(
            'idx-city-album_id',
            'city'
        );

        $this->dropColumn('city', 'album_id');
    }
}
