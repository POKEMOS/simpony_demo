<?php

use yii\db\Migration;

/**
 * Handles adding callac to table `contacts`.
 */
class m170619_165822_add_callac_column_to_contacts_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('contacts', 'callac', $this->string(255));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('contacts', 'callac');
    }
}
