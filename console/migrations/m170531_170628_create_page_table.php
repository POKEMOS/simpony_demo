<?php

use yii\db\Migration;

/**
 * Handles the creation of table `page`.
 */
class m170531_170628_create_page_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('page', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'title' => $this->string(255),
            'description_long' => $this->text(),
            'description_page' => $this->text(),
            'description_sales' => $this->text(),
            'photo' => $this->integer(),
        ],'DEFAULT CHARSET=utf8');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('page');
    }
}
