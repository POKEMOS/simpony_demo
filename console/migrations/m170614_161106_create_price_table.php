<?php

use yii\db\Migration;

/**
 * Handles the creation of table `price`.
 */
class m170614_161106_create_price_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('price', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'price' => $this->string(255),
            'type' => $this->integer()->defaultValue(0),
        ],'DEFAULT CHARSET=utf8');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('price');
    }
}
