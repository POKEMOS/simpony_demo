<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Page */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'title',
            'description_long:ntext',
            'description_page:ntext',
            'description_sales:ntext',
            [
                'label'  => 'Фотография',
                'value'  => Yii::$app->imagemanager->getImagePath($model->photo, 200, 200,'inset'),
                'format' => ['image'],
            ],
        ],
    ]) ?>

</div>
