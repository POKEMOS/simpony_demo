<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `city`.
 */
class m170619_165330_add_columns_column_to_city_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('city', 'description', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('city', 'description');
    }
}
