<?php

use yii\db\Migration;

/**
 * Handles adding op to table `price`.
 */
class m170619_172020_add_op_column_to_price_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('price', 'old_price', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('price', 'old_price');
    }
}
