<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "comments".
 *
 * @property integer $id
 * @property string $name
 * @property string $city
 * @property string $description
 * @property integer $photo
 * @property string $date
 */
class Comments extends \yii\db\ActiveRecord
{

    public $video_file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description','video'], 'string'],
            [['photo'], 'integer'],
            [['name', 'city', 'date'], 'string', 'max' => 255],
            [['description','photo','name', 'city', 'date'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'city' => 'City',
            'description' => 'Description',
            'photo' => 'Photo',
            'date' => 'Date',
        ];
    }

    public function upload()
    {
        $this->video = uniqid($this->video_file->baseName,true).'.'.$this->video_file->extension;
        if ($this->validate()) {
            $this->video_file->saveAs(Yii::getAlias('@frontend') .'/web/img/video/' . $this->video);
            return true;
        } else {
            return false;
        }
    }
}
