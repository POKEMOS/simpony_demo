<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PriceBlocks */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Price Blocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-blocks-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'label'  => 'photo',
                'value'  => Yii::$app->imagemanager->getImagePath($model->img, 200, 200,'inset'),
                'format' => ['image'],
            ],
            'description:ntext',
            [
                'label' => 'page',
                'value' => $model->page->name
            ],
        ],
    ]) ?>

</div>
