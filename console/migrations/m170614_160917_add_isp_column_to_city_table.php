<?php

use yii\db\Migration;

/**
 * Handles adding isp to table `city`.
 */
class m170614_160917_add_isp_column_to_city_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('city', 'is_process', $this->integer()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('city', 'is_process');
    }
}
