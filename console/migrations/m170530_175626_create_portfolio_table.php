<?php

use yii\db\Migration;

/**
 * Handles the creation of table `portfolio`.
 * Has foreign keys to the tables:
 *
 * - `city`
 * - `album`
 */
class m170530_175626_create_portfolio_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('portfolio', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer(),
            'photo' => $this->integer(),
        ],'DEFAULT CHARSET=utf8');

        // creates index for column `city_id`
        $this->createIndex(
            'idx-portfolio-city_id',
            'portfolio',
            'city_id'
        );

        // add foreign key for table `city`
        $this->addForeignKey(
            'fk-portfolio-city_id',
            'portfolio',
            'city_id',
            'city',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `city`
        $this->dropForeignKey(
            'fk-portfolio-city_id',
            'portfolio'
        );

        // drops index for column `city_id`
        $this->dropIndex(
            'idx-portfolio-city_id',
            'portfolio'
        );

        $this->dropTable('portfolio');
    }
}
