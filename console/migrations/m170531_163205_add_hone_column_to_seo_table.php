<?php

use yii\db\Migration;

/**
 * Handles adding hone to table `seo`.
 */
class m170531_163205_add_hone_column_to_seo_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('seo', 'h1', $this->string(255));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('seo', 'h1');
    }
}
