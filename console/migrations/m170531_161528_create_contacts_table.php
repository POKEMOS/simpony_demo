<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contacts`.
 */
class m170531_161528_create_contacts_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('contacts', [
            'id' => $this->primaryKey(),
            'phone' => $this->string(255),
            'email' => $this->string(255),
            'addr' => $this->string(255),
        ],'DEFAULT CHARSET=utf8');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('contacts');
    }
}
