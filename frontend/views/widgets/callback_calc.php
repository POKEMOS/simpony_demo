<!--Модальное окно Расчитать стоимость-->
<div class="remodal-wrapper remodal-is-closed" style="display: none;" class="form-send">
    <div class="remodal remodal-is-initialized remodal-is-closed" data-remodal-id="secondModalschet" data-remodal-options="hashTracking: false,closeOnConfirm: false" tabindex="-1">

        <div class="formArea">

            <form id="secondForm" class="form js-send-form form-send form-calc-c" method="post" action="/callback" enctype="multipart/form-data" autocomplete="off">

                <p class="msgs"></p>
                <div class="midd_text">
                    РАССЧИТАТЬ СТОИМОСТЬ
                </div>
                <img src="/img/logo.png" alt="Цвет">

                <input type="hidden" name="type" value="other">
                <fieldset style="width: 100%;" class="form-fieldset ui-input __first">
                    <input type="text" tabindex="0" placeholder="Ваше имя" name="name" required>
                </fieldset>
                <fieldset style="width: 100%;" class="form-fieldset ui-input __first">
                    <input type="text" tabindex="0" placeholder="Ваш email" name="email">
                </fieldset>
                <fieldset style="width: 100%;" class="form-fieldset ui-input __second">
                    <input required type="tel" id="phone" tabindex="0" placeholder="Ваш телефон" name="phone" required>
                    <input type="text" tabindex="0" placeholder="Ваш комментарий" name="comment">
                </fieldset>
                <fieldset style="width: 100%;" class="form-fieldset ui-input __second schet">
                    <a id="chose_file" href="#">Прикрепить файл</a>
                    <span id="chose_file_text"></span>
                    <input id="chose_file_input" name="imagefile" type="file" />
                </fieldset>

                <input name="formInfo" class="formInfo" type="hidden" value="ОТПРАВИТЬ">

                <div style="width: 100%;" class="form-footer">
                    <input type="submit" class="formBtn" onclick="yaCounter44951161.reachGoal('Call_Order'); return true;" value="Заказать звонок">
                </div>

            </form>
        </div>
    </div>
</div>
<!--Модальное окно Расчитать стоимость-->