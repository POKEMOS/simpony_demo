<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use common\models\City;
/* @var $this yii\web\View */
/* @var $searchModel common\models\PortfolioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Portfolios';
$this->params['breadcrumbs'][] = $this->title;

$cities_mod = City::find()->all();
$cities = ArrayHelper::map($cities_mod,'id',function($data) {
    return $data->album->name.' - '.$data->name;
});
?>
<div class="portfolio-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Portfolio', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute'=>'city_id',
                'filter'=>$cities,
                'value' => function ($data) {
                    return $data->city->album->name.' - '.$data->city->name;
                },
            ],
            [
                'attribute' => 'photo',
                'format' => 'html',
                'filter' => false,
                'value' => function ($data) {
                    return Html::img(Yii::$app->imagemanager->getImagePath($data->photo, 200, 200,'inset'));
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
