<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "price_blocks".
 *
 * @property integer $id
 * @property integer $img
 * @property string $description
 * @property integer $price
 *
 * @property Price $price0
 */
class PriceBlocks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'price_blocks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['img', 'price'], 'integer'],
            [['description'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'img' => 'Img',
            'description' => 'Description',
            'price' => 'Price',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(PricePage::className(), ['id' => 'price']);
    }
}
