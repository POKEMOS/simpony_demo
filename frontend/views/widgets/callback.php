<?
use yii\widgets\MaskedInput;
?>
<!--Модальное окно-->
<div class="remodal-wrapper remodal-is-closed" style="display: none;" class="form-send">
    <div class="remodal remodal-is-initialized remodal-is-closed" data-remodal-id="secondModal2" data-remodal-options="hashTracking: false,closeOnConfirm: false" tabindex="-1">

        <div class="formArea">

            <form id="secondForm" class="form js-send-form form-send" method="post" action="/callback" autocomplete="off">

                <p class="msgs"></p>
                <div class="midd_text">
                    Обратный звонок
                </div>
                <img src="/img/logo.png" alt="Цвет">

                <input type="hidden" name="type" value="other">
                <fieldset style="width: 100%;" class="form-fieldset ui-input __first">
                    <input required type="text" tabindex="0" placeholder="Ваше имя" name="name">
                </fieldset>
                <fieldset style="width: 100%;" class="form-fieldset ui-input __first">
                    <input type="text" tabindex="0" placeholder="Ваш email" name="email">
                </fieldset>
                <fieldset style="width: 100%;" class="form-fieldset ui-input __second">
                    <input type="text" tabindex="0" placeholder="Ваш телефон" name="phone" required>
                    <input type="text" tabindex="0" placeholder="Ваш комментарий" name="comment">
                </fieldset>

                <input name="formInfo" class="formInfo" type="hidden" value="ОТПРАВИТЬ">

                <div style="width: 100%;" class="form-footer">
                    <input type="submit" class="formBtn" onclick="yaCounter44951161.reachGoal('Call_Order'); return true;" value="Заказать звонок">
                </div>

            </form>
        </div>
    </div>
</div>
<!--Модальное окно-->