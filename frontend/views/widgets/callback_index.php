<?
use yii\widgets\MaskedInput;
?>
<div class="m_b_cons_call">
    <form action="/callback" method="post" >
        <div class="flex_mid">
            <div>
                <input placeholder="Имя" type="text" name="name" required>
            </div>
            <div>
                <input placeholder="Ваш телефон" type="text" name="phone" required>
            </div>
            <div>
                <input placeholder="Ваш вопрос" name="comment" type="text">
            </div>
        </div>
        <div class="flex_mid">
            <div class="button_footer">
                <input type="submit" value="Отправить" onclick="yaCounter44951161.reachGoal('Question_Send'); return true;">
            </div>
        </div>
    </form>
</div>