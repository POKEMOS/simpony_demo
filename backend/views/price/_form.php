<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\PricePage;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Price */
/* @var $form yii\widgets\ActiveForm */

$types = [
    0 => 'Полный прайс',
    1 => 'Прайс полы',
    2 => 'Прайс стены',
];

$pages_mod = PricePage::find()->all();
$pages = ArrayHelper::map($pages_mod,'id','name');

?>

<div class="price-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'old_price')->textInput(['type' => 'number']) ?>

    <?= $form->field($model, 'page')->dropDownList($pages,['prompt' => 'Не указывать']) ?>

    <?= $form->field($model, 'type')->dropDownList($types,['prompt' => 'Не указывать']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
