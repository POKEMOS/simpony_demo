<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PricePage */

$this->title = 'Create Price Page';
$this->params['breadcrumbs'][] = ['label' => 'Price Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-page-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
