<?php

use yii\db\Migration;

/**
 * Handles the creation of table `slider`.
 */
class m170531_180311_create_slider_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('slider', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'photo' => $this->string(255),
        ],'DEFAULT CHARSET=utf8');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('slider');
    }
}
