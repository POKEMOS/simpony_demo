<?php

use yii\db\Migration;

/**
 * Handles the creation of table `price_page`.
 */
class m170619_143913_create_price_page_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('price_page', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('price_page');
    }
}
