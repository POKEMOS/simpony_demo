<section class="sf_top_text unic_1 dom">
    <div class="middle">
        <h1><?=$h1?></h1>
        <?=yii::$app->controller->renderPartial('//widgets/footer_callback')?>
        <!--BLOCK-->
        <div class="text_top-ramka">
            <div>
                <?=$description_long?>

            </div>
        </div>
        <!--BLOCK-->
    </div>
</section>
<!--BLOCK-->
<section class="sf_m_box_utp montaj">
    <div class="middle">
        <div class="line_home">
        </div>
        <img src="<?=Yii::$app->imagemanager->getImagePath($img, 5000, 5000,'inset')?>" alt="">
        <div class="sf_utp_text">
            <?=$description_page?>
			<button data-remodal-target="secondModal2">Оставить заявку</button>																		
        </div>
    </div>
</section>
<!--BLOCK-->
<section class="sf_m_box_advantage">
    <div class="middle anim_plus">
        <h3>НАШИ ПРЕИМУЩЕСТВА</h3>
        <div class="flex_mid">
            <div ><img src="/img/sf_ad_box1.png" alt="pictures">
                <div>
                    <p class="big">НИЗКИЕ ЦЕНЫ</p>
                    <p>Мы гарантируем Вам <br>самую низкую цену</p>
                </div>
            </div>
            <div >
                <img src="/img/sf_ad_box3.png" alt="pictures">
                <div>
                    <p class="big">ОПЫТНЫЕ <br> СПЕЦИАЛИСТЫ</p>
                    <p>Сотрудники компании — <br>специалисты с <br>многолетним опытом</p>
                </div>
            </div>
        </div>
        <div class="flex_mid">
            <div >
                <img src="/img/sf_ad_box2.png" alt="pictures">
                <div>
                    <p class="big">ГАРАНТИЯ <br> КАЧЕСТВА</p>
                    <p>Мы предоставляем <br>Вам гарантию <br>на 5 лет</p>
                </div>
            </div>
            <div >
                <img src="/img/sf_ad_box4.png" alt="pictures">
                <div>
                    <p class="big">ЛЮБЫЕ <br>СПОСОБЫ ОПЛАТЫ</p>
                    <p>Оплачивайте наши услуги <br>любым доступным <br>Вам способом</p>
                </div>
            </div>
        </div>
    </div>
</section>
<?=yii::$app->controller->renderPartial('//widgets/works',['works' => $works])?>
<section class="m_box_advantage gar">
    <div class="middle anim_plus">
        <h3>ВАШИ ГАРАНТИИ</h3>
        <div class="flex_mid">
            <div><img src="/img/sf_gar_box1.png" alt="pictures">
                <p class="gar_text">Прозрачная<br>строительная<br>смета</p>
            </div>
            <div><img src="/img/sf_gar_box2.png" alt="pictures">
                <p class="gar_text">Поэтапная<br>сдача работ</p>
            </div>
            <div><img src="/img/sf_gar_box3.png" alt="pictures">
                <p class="gar_text">Точное выполнение<br>календарного<br>плана работ</p>
            </div>
            <div><img src="/img/sf_gar_box4.png" alt="pictures">
                <p class="gar_text">Онлайн-наблюдение<br>за строительством</p>
            </div>
        </div>
    </div>
</section>
<section class="sf_m_box_estimate">
    <div class="middle">
        <h2>Есть готовый проект дома?</h2>
        <span>Оценим и пришлем смету в течение 1 рабочего дня!</span>
        <button type="submit" data-remodal-target="secondModalschet" >Рассчитать</button>
    </div>
</section>
<section class="sf_m_box_other remont dom">
    <div class="middle">
        <h3>стоимость кровельных работ</h3>
        <div class="dom_other_tabs_cell">
				 
                <div>Средняя стоимость ремонта плоских кровель</div>
                <table>
                    
                    <tr>
                        <td>Наименование</td>
                        <td>Ед-ца</td>
                        <td>Материалы/Механизмы</td>
                        <td>Работы</td>
                        <td>Итого</td>
                    </tr>
                    <tr class="n">
                        <td>Ремонт с битумным материалом и выполненной разуклонкой (включая материалы) <strong>с применением битумных рулонных материалов однослойное решение</strong></td>
                        <td>м2</td>
                        <td>от 535,00</td>
                        <td>от 185,00</td>
                        <td>от 720,00</td>
                    </tr>
                    <tr class="n">
                        <td>Ремонт с битумным материалом и выполненной разуклонкой (включая материалы) <strong>с применением битумных рулонных материалов двухслойное решение</strong></td>
                        <td>м2</td>
                        <td>от 495,00</td>
                        <td>от 250,00</td>
                        <td>от 745,00</td>
                    </tr>
                    <tr class="n">
                        <td>Ремонт с изношенным битумным материалом и выполненной разуклонкой (включая материалы) <strong>с применением технологии — мембранная кровля</strong></td>
                        <td>м2</td>
                        <td>от 575,00</td>
                        <td>от 150,00</td>
                        <td>от 725,00</td>
                    </tr>
                </table>
                <div>Средняя стоимость новых плоских кровель</div>
                <table>
                    
                    <tr>
                        <td>Наименование</td>
                        <td>Ед-ца</td>
                        <td>Материалы/Механизмы</td>
                        <td>Работы</td>
                        <td>Итого</td>
                    </tr>
                    <tr class="n">
                        <td>Гидроизоляция, утепление пенополистирол 100мм, разуклонка керамзит, стяжка 40-50 мм, праймерс <strong>применением битумных рулонных материалов однослойное решение</strong></td>
                        <td>м2</td>
                        <td>от 1650,00</td>
                        <td>от 450,00</td>
                        <td>от 2 100,00</td>
                    </tr>
                    <tr class="n">
                        <td>Гидроизоляция, утепление пенополистирол 100мм, разуклонка керамзит, стяжка 40-50 мм, праймерс <strong>применением битумных рулонных материалов двухслойное решение</strong></td>
                        <td>м2</td>
                        <td>от 1645,00</td>
                        <td>от 545,00</td>
                        <td>от 2190,00</td>
                    </tr>
                    <tr class="n">
									  
													   
                        <td>Гидроизоляция, утепление минвата 100мм, разуклонка OSB-плита с деревянной обрешеткой, <strong>с применением технологии мембранная кровля</strong></td>
                        <td>м2</td>
                        <td>от 1750,00</td>
                        <td>от 440,00</td>
                        <td>от 2 190,00</td>
                    </tr>
                    <tr class="n">
                        <td>Плоская кровля с выполненной разуклонкой профлист: гидроизоляция, утепление минвата 100мм, <strong>с применением технологии мембранная кровля</strong></td>
                        <td>м2</td>
                        <td>от 1345,00</td>
                        <td>от 350,00</td>
                        <td>от 1 695,00</td>
									  
													   
												
                    </tr>
                </table>

                </div>

        <button type="submit" data-remodal-target="secondModalschet">рассчитать стоимость</button>
    </div>
</section>
<section class="sf_m_box_consultation">
    <div class="middle">
        <h2>ОСТАЛИСЬ ВОПРОСЫ?
            <span>Мы с радостью на них ответим. Заполните, пожалуйста, форму!</span>
        </h2>
        <?=yii::$app->controller->renderPartial('//widgets/callback_index')?>
    </div>
</section>
<section class="sf_m_box_outline">
    <div class="middle">
        <h3>СХЕМА РАБОТЫ</h3>
        <div class="flex_mid">
            <div>

                <img src="/img/sf_sh_box1.png" alt="">
                <p>Отправка<br>заявки</p>
                <p>Вы отправляете<br>заявку на сайте</p>
            </div>
            <div>

                <img src="/img/sf_sh_box2.png" alt="">
                <p>Коммерческое <br>предложение </p>
                <p>Мы <br>перезваниваем Вам<br>и обсуждаем проект</p>
            </div>
            <div>
                <img src="/img/sf_sh_box3.png" alt="">
                <p>Утверждение<br>сметы</p>
                <p>Подготавливаем<br>проект<br>и отправляем смету</p>
            </div>
            <div>
                <img src="/img/sf_sh_box4.png" alt="">
                <p>Договор</p>
                <p>Подписываем<br>договор<br>и производим<br>строительные работы</p>
            </div>
            <div>
                <img src="/img/sf_sh_box5.png" alt="">
                <p>Сдача проекта</p>
                <p>Мы точно в срок<br>сдаем Вам<br>качественную работу</p>
            </div>
        </div>
    </div>
</section>

<section class="sf_m_box_carusel_img sert">
    <div class="middle">
        <h3>СЕРТИФИКАТЫ</h3>
        <div class="owl-carousel owl-theme" id="sl_dock">
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat.jpg">
                    <img src="/img/sertificat.jpg" alt="">
                    <a href="#">Свидетельство</a></a>
            </div>
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat2.jpg">
                    <img src="/img/sertificat2.jpg" alt="">
                    <a href="#">Приложение 1</a></a>
            </div>
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat3.jpg">
                    <img src="/img/sertificat3.jpg" alt="">
                    <a href="#">Приложение 2</a></a>
            </div>
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat4.jpg">
                    <img src="/img/sertificat4.jpg" alt="">
                    <a href="#">Приложение 3</a>
                </a>
            </div>
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat5.jpg">
                    <img src="/img/sertificat5.jpg" alt="">
                    <a href="#">Приложение 4</a>
                </a>
            </div>
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat6.jpg">
                    <img src="/img/sertificat6.jpg" alt="">
                    <a href="#">Приложение 5</a>
                </a>
            </div>
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat7.jpg">
                    <img src="/img/sertificat7.jpg" alt="">
                    <a href="#">Приложение 6</a>
                </a>
            </div>
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat8.jpg">
                    <img src="/img/sertificat8.jpg" alt="">
                    <a href="#">Приложение 7</a>
                </a>
            </div>
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat9.jpg">
                    <img src="/img/sertificat9.jpg" alt="">
                    <a href="#">Приложение 8</a>
                </a>
            </div>
        </div>
    </div>
</section>
<?=$comments?>