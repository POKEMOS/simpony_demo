<?php

use yii\db\Migration;

/**
 * Handles the creation of table `price_blocks`.
 */
class m170619_151959_create_price_blocks_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('price_blocks', [
            'id' => $this->primaryKey(),
            'img' => $this->integer(),
            'description' => $this->text(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('price_blocks');
    }
}
