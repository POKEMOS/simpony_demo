<section class="sf_top_text unic_1 pol">
    <div class="middle">
        <h1><?=$h1?></h1>
        <?=yii::$app->controller->renderPartial('//widgets/footer_callback')?>
        <!--BLOCK-->
        <div class="text_top-ramka">
            <div>
                <?=$description_long?>

            </div>
        </div>
        <!--BLOCK-->
    </div>
</section>
<!--BLOCK-->
<section class="sf_m_box_utp montaj">
    <div class="middle">
        <div class="line_home">
        </div>
        <img src="<?=Yii::$app->imagemanager->getImagePath($img, 5000, 5000,'inset')?>" alt="">
        <div class="sf_utp_text">
            <?=$description_page?>
			<button data-remodal-target="secondModal2">Оставить заявку</button>																		
        </div>
    </div>
</section>
<!--BLOCK-->
<section class="sf_m_box_advantage">
    <div class="middle anim_plus">
        <h3>НАШИ ПРЕИМУЩЕСТВА</h3>
        <div class="flex_mid">
            <div ><img src="/img/sf_ad_box1.png" alt="pictures">
                <div>
                    <p class="big">НИЗКИЕ ЦЕНЫ</p>
                    <p>Мы гарантируем Вам <br>самую низкую цену</p>
                </div>
            </div>
            <div >
                <img src="/img/sf_ad_box3.png" alt="pictures">
                <div>
                    <p class="big">ОПЫТНЫЕ <br> СПЕЦИАЛИСТЫ</p>
                    <p>Сотрудники компании — <br>специалисты с <br>многолетним опытом</p>
                </div>
            </div>
        </div>
        <div class="flex_mid">
            <div >
                <img src="/img/sf_ad_box2.png" alt="pictures">
                <div>
                    <p class="big">ГАРАНТИЯ <br> КАЧЕСТВА</p>
                    <p>Мы предоставляем <br>Вам гарантию <br>на 5 лет</p>
                </div>
            </div>
            <div >
                <img src="/img/sf_ad_box4.png" alt="pictures">
                <div>
                    <p class="big">ЛЮБЫЕ <br>СПОСОБЫ ОПЛАТЫ</p>
                    <p>Оплачивайте наши услуги <br>любым доступным <br>Вам способом</p>
                </div>
            </div>
        </div>
    </div>
</section>
<?=yii::$app->controller->renderPartial('//widgets/works',['works' => $works])?>
<section class="m_box_advantage gar">
    <div class="middle anim_plus">
        <h3>ВАШИ ГАРАНТИИ</h3>
        <div class="flex_mid">
            <div><img src="/img/sf_gar_box1.png" alt="pictures">
                <p class="gar_text">Прозрачная<br>строительная<br>смета</p>
            </div>
            <div><img src="/img/sf_gar_box2.png" alt="pictures">
                <p class="gar_text">Поэтапная<br>сдача работ</p>
            </div>
            <div><img src="/img/sf_gar_box3.png" alt="pictures">
                <p class="gar_text">Точное выполнение<br>календарного<br>плана работ</p>
            </div>
            <div><img src="/img/sf_gar_box4.png" alt="pictures">
                <p class="gar_text">Онлайн-наблюдение<br>за строительством</p>
            </div>
        </div>
    </div>
</section>
<section class="sf_m_box_estimate">
    <div class="middle">
        <h2>Есть готовый проект дома?</h2>
        <span>Оценим и пришлем смету в течение 1 рабочего дня!</span>
        <button type="submit" data-remodal-target="secondModalschet">Рассчитать</button>
    </div>
</section>
<section class="sf_m_box_other remont dom">
    <div class="middle">
        <h3>стоимость заливки полов</h3>
        <div class="text_top-ramka">
            <div>
                <p>
                    При заказе заливки полов, необходимо определиться с типом покрытия и технологии. Наши специалисты помогут выбрать оптимальный вариант для вашего проекта.
                </p>
               <ul class="list_other">
                            <li>спортзалы и фитнес клубы;</li>
                            <li>медицинские учреждения;</li>
                            <li>торговые и выставочные комплексы;</li>
                            <li>электростанции;</li>
                            <li>предприятия пищевой промышленности;</li>
                            <li>квартиры, коттеджи, помещения с декоративной нагрузкой пола;</li>
                        </ul>
            </div>
        </div>
        <div class="remont_other_tabs_cell">
                   <table>
                       <tr>
                          <td class="name">Вид пола</td>
                          <td class="name">Материал</td>  
                          <td class="name">Работа</td>  
                       </tr>
                       <tr>
                           <td>Бетонный пол черновой (без упрочнения)</td>
                           <td>от 400р./м2</td>
                           <td>от 200р./м2</td>
                       </tr>
                       <tr>
                           <td>Бетонный пол с упрочнением</td>
                           <td>от 450р./м2</td>
                           <td>от 250р./м2</td>
                       </tr>
                       <tr>
                           <td>Наливной полимерный пол</td>
                           <td>от 990р./м2</td>
                           <td>от 450р./м2</td>
                       </tr>
                       <tr>
                           <td>Полусухая стяжка</td>
                           <td>от 400р./м2</td>
                           <td>от 150р./м2</td>
                       </tr>
                   </table>
                    
                </div>

        <button type="submit" data-remodal-target="secondModalschet">рассчитать стоимость</button>
    </div>
</section>
<section class="sf_m_box_consultation">
    <div class="middle">
        <h2>ОСТАЛИСЬ ВОПРОСЫ?
            <span>Мы с радостью на них ответим. Заполните, пожалуйста, форму!</span>
        </h2>
        <div class="m_b_cons_call">
            <form action="" >
                <div class="flex_mid">
                    <div>
                        <input id="name" placeholder="Имя" type="text">
                    </div>
                    <div>

                        <input id="phone" placeholder="Ваш телефон" type="text">
                    </div>
                    <div>
                        <input id="text" placeholder="Ваш вопрос" type="text">
                    </div>
                </div>
                <div class="flex_mid">
                    <div class="button_footer">
                        <input type="submit" value="Отправить">
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<section class="sf_m_box_outline">
    <div class="middle">
        <h3>СХЕМА РАБОТЫ</h3>
        <div class="flex_mid">
            <div>

                <img src="/img/sf_sh_box1.png" alt="">
                <p>Отправка<br>заявки</p>
                <p>Вы отправляете<br>заявку на сайте</p>
            </div>
            <div>

                <img src="/img/sf_sh_box2.png" alt="">
                <p>Коммерческое <br>предложение </p>
                <p>Мы <br>перезваниваем Вам<br>и обсуждаем проект</p>
            </div>
            <div>
                <img src="/img/sf_sh_box3.png" alt="">
                <p>Утверждение<br>сметы</p>
                <p>Подготавливаем<br>проект<br>и отправляем смету</p>
            </div>
            <div>
                <img src="/img/sf_sh_box4.png" alt="">
                <p>Договор</p>
                <p>Подписываем<br>договор<br>и производим<br>строительные работы</p>
            </div>
            <div>
                <img src="/img/sf_sh_box5.png" alt="">
                <p>Сдача проекта</p>
                <p>Мы точно в срок<br>сдаем Вам<br>качественную работу</p>
            </div>
        </div>
    </div>
</section>

<section class="sf_m_box_carusel_img sert">
    <div class="middle">
        <h3>СЕРТИФИКАТЫ</h3>
        <div class="owl-carousel owl-theme" id="sl_dock">
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat.jpg">
                    <img src="/img/sertificat.jpg" alt="">
                    <a href="#">Свидетельство</a></a>
            </div>
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat2.jpg">
                    <img src="/img/sertificat2.jpg" alt="">
                    <a href="#">Приложение 1</a></a>
            </div>
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat3.jpg">
                    <img src="/img/sertificat3.jpg" alt="">
                    <a href="#">Приложение 2</a></a>
            </div>
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat4.jpg">
                    <img src="/img/sertificat4.jpg" alt="">
                    <a href="#">Приложение 3</a>
                </a>
            </div>
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat5.jpg">
                    <img src="/img/sertificat5.jpg" alt="">
                    <a href="#">Приложение 4</a>
                </a>
            </div>
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat6.jpg">
                    <img src="/img/sertificat6.jpg" alt="">
                    <a href="#">Приложение 5</a>
                </a>
            </div>
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat7.jpg">
                    <img src="/img/sertificat7.jpg" alt="">
                    <a href="#">Приложение 6</a>
                </a>
            </div>
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat8.jpg">
                    <img src="/img/sertificat8.jpg" alt="">
                    <a href="#">Приложение 7</a>
                </a>
            </div>
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat9.jpg">
                    <img src="/img/sertificat9.jpg" alt="">
                    <a href="#">Приложение 8</a>
                </a>
            </div>
        </div>
    </div>
</section>
<section class="m_box_carusel_text">
    <div class="middle flex-col">
        <h3>ОТЗЫВЫ</h3>
        <div class="owl-carousel owl-theme" id="sl_otziv">
            <div class="item">
                <img src="/img/ONBE7Y0.png" alt="">
                <div class="ramka">
                    <div>Ирина, Москва <span>25.04.2017</span></div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

                </div>
            </div>
            <div class="item">
                <img src="/img/ONBE7Y0.png" alt="">
                <div class="ramka">
                    <div>Ирина, Москва <span>25.04.2017</span></div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

                </div>
            </div>
            <div class="item">
                <img src="/img/ONBE7Y0.png" alt="">
                <div class="ramka">
                    <div>Ирина, Москва <span>25.04.2017</span></div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

                </div>
            </div>
        </div>
    </div>
</section>
<?=$comments?>