-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 31, 2017 at 08:42 
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sf`
--

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE `album` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `photo` int(11) DEFAULT NULL,
  `hidden` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`id`, `name`, `photo`, `hidden`) VALUES
(1, 'Архитектурное освещение', 3, 0),
(2, 'Монтаж вентилируемых фасадов', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `callback`
--

CREATE TABLE `callback` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `comment` text,
  `date` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `callback`
--

INSERT INTO `callback` (`id`, `name`, `email`, `phone`, `comment`, `date`) VALUES
(1, 'f', 'fewfwefew@fwe.fwe', '+7 (234)-324-23-42', 'frfrgergerge', '20.05.2017'),
(2, 'ger', 'gerger@gre.gger', '+7 (234)-234-23-42', 'gergerge', '31.05.2017'),
(3, 'Mike', NULL, '+7 (111)-111-21-21', NULL, '31.May.17'),
(4, 'Mike', NULL, '+7 (111)-111-21-21', NULL, '31.05.17171717'),
(5, 'Mike', NULL, '+7 (111)-111-21-21', NULL, '31.05.2017'),
(6, 'майк', NULL, '+7 (333)-333-33-23', NULL, '31.05.2017'),
(7, 'Mkk', NULL, '+7 (123)-456-78-90', NULL, '31.05.2017'),
(8, 'Mkk', NULL, '+7 (123)-456-78-90', NULL, '31.05.2017'),
(9, 'Mkk', NULL, '+7 (123)-456-78-90', NULL, '31.05.2017'),
(10, 'Mkk', NULL, '+7 (123)-456-78-90', NULL, '31.05.2017'),
(11, 'Mkk', NULL, '+7 (123)-456-78-90', NULL, '31.05.2017'),
(12, 'Майк', 'mike@mikmk._', '+7 (123)-132-13-12', NULL, '31.05.2017'),
(13, 'grge', 'mike@mike.mk', '+7 (323)-432-42-34', NULL, '31.05.2017'),
(14, 'grge', 'mike@mike.mk', '+7 (323)-432-42-34', NULL, '31.05.2017'),
(15, 'mike', 'mike@mike.mk', '+7 (334)-234-23-42', 'Hello World!', '31.05.2017'),
(16, 'Mike', NULL, '+453453', 'fewfwefwe', '31.05.2017'),
(17, 'mikki', '', '+73242342', '', '31.05.2017'),
(18, 'fwefw', 'fwefw', 'fwefwefw', 'fwefwe', '31.05.2017'),
(19, 'gerg', NULL, '543534', NULL, '31.05.2017'),
(20, 'gwr', NULL, '5453453', '', '31.05.2017'),
(21, 'gerge', NULL, '5345345345', 'hello world', '31.05.2017'),
(22, 'Mikkki', NULL, '111212', NULL, '31.05.2017'),
(23, 'Васька', NULL, '1111111', NULL, '31.05.2017'),
(24, 'тест имя', 'тест почта', 'тест телефон', 'тест коммент', '31.05.2017'),
(25, 'тест имя', 'тест почта', 'тест телефон', 'тест коммент', '31.05.2017'),
(26, 'тест имя', 'тест почта', 'тест телефон', 'тест коммент', '31.05.2017'),
(27, 'тест имя', 'тест почта', 'тест телефон', 'тест коммент', '31.05.2017'),
(28, 'wgwefg', 'reg', 'gerfg', 'grf', '31.05.2017'),
(29, 'wgwefg', 'reg', 'gerfg', 'grf', '31.05.2017'),
(30, 'wfgwe', 'grgeg', 'gergw', 'gergerge', '31.05.2017'),
(31, 'wfgwe', 'grgeg', 'gergw', 'gergerge', '31.05.2017'),
(32, 'wfgwe', 'grgeg', 'gergw', 'gergerge Документ: sf.dev/img/upload/592ee674bb1cb2.32645992_beton_payn.jpg', '31.05.2017'),
(33, 'wfgwe', 'grgeg', 'gergw', 'gergerge Документ: http://sf.dev/img/upload/592ee6aca6e401.65330091_beton_payn.jpg', '31.05.2017'),
(34, 'wfgwe', 'grgeg', 'gergw', 'gergerge Документ: sf.dev/img/upload/592ee6c1d1ce54.83209360_beton_payn.jpg', '31.05.2017'),
(35, 'fwfewgwer', 'rgerfgwe', 'gwgherhwergerg', 't2t34tgebgef Документ: sf.dev/img/upload/592ee746afc263.20779384_dub_stirling.jpg', '31.05.2017'),
(36, 'fwfewgwer', 'rgerfgwe', 'gwgherhwergerg', 't2t34tgebgef Документ: sf.dev/img/upload/592ee74ad143b4.78670660_dub_stirling.jpg', '31.05.2017'),
(37, 'fwfewgwer', 'rgerfgwe', 'gwgherhwergerg', 't2t34tgebgef Документ: sf.dev/img/upload/592ee7d0ccff55.45831654_dub_stirling.jpg', '31.05.2017'),
(38, 'fwe', NULL, 'fwe', '', '31.05.2017'),
(39, 'fwe', NULL, 'fwe', '', '31.05.2017');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `album_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `album_id`) VALUES
(1, 'г Людиново, Бригантина', 1),
(2, 'Брянская обл, п. Дятьково', 2);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `description` text,
  `photo` int(11) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `name`, `city`, `description`, `photo`, `date`) VALUES
(1, 'fwe', 'few', 'fwe', 3, '18.05.2017'),
(2, 'Ирина', 'Москва', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 4, '19.05.2017');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `addr` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `phone`, `email`, `addr`) VALUES
(1, 'Москва +7 (909) 649-649-0 ', 'TP@SF57.RU ', 'Москва ул. Часовая 28'),
(2, '+7 (967) 152-13-08', 'ART@SF57.RU', ''),
(3, 'Орел +7 (969) 018-03-34', 'INFO@SF57.RU', '');

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `ext` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `imagemanager`
--

CREATE TABLE `imagemanager` (
  `id` int(10) UNSIGNED NOT NULL,
  `fileName` varchar(128) NOT NULL,
  `fileHash` varchar(32) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  `createdBy` int(10) UNSIGNED DEFAULT NULL,
  `modifiedBy` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `imagemanager`
--

INSERT INTO `imagemanager` (`id`, `fileName`, `fileHash`, `created`, `modified`, `createdBy`, `modifiedBy`) VALUES
(2, '139447713.jpg', 'DZpJdD3VOLaDgMqZEfDPUStQW5olPqKm', '2017-05-30 21:05:45', '2017-05-30 21:05:45', NULL, NULL),
(3, 'maxresdefault.jpg', 'CDILibxrgPQswu75GLKR8YWFJG6yrRUB', '2017-05-30 21:22:18', '2017-05-30 21:22:18', NULL, NULL),
(4, 'ONBE7Y0.png', '82WyD6rv1M42dKalkGhwLd1iKTA7l6Pd', '2017-05-30 21:42:11', '2017-05-30 21:42:11', NULL, NULL),
(5, '20160314-194543.jpg', 'mavsLQOCzWyR_cXsRdJAGV8nDBkQIY5v', '2017-05-30 22:40:54', '2017-05-30 22:40:54', NULL, NULL),
(6, '20160419-203636.jpg', 'RSQzqIox9HQX5A81FqMw-1SzGg6vEdfX', '2017-05-30 22:41:03', '2017-05-30 22:41:03', NULL, NULL),
(7, '20160701-215912.jpg', 'GJmb6Hj1pFlCGUVOygHAHQgeL2slJc1v', '2017-05-30 22:41:06', '2017-05-30 22:41:06', NULL, NULL),
(8, '20160717-111055.jpg', 'XgmUIxXzgjF03b3SyzVqfASk-xiC1Tpy', '2017-05-30 22:41:19', '2017-05-30 22:41:19', NULL, NULL),
(9, '20160805-211112.jpg', 'gSnO1Tz2cTO_OO6CdCM2cV7qzeqZrzGt', '2017-05-30 22:41:27', '2017-05-30 22:41:27', NULL, NULL),
(10, '20160805-211140.jpg', 'ICETGWymHITk0qsJ9k6Tl1kgZ1wCLqsB', '2017-05-30 22:41:28', '2017-05-30 22:41:28', NULL, NULL),
(11, '20160805-211148.jpg', '8VUxMLSVAL5rwvrmu3z-LPsWSdYxjmVn', '2017-05-30 22:41:28', '2017-05-30 22:41:28', NULL, NULL),
(12, '20160810-110059 -1-.jpg', '_0yrd5ccZnJkAH2TC_QSHSqHQHHy4BlQ', '2017-05-30 22:41:28', '2017-05-30 22:41:28', NULL, NULL),
(13, '20160810-110059.jpg', 'GYO4C6rT1-l46ijPDNVQZdIScZ4HeHtk', '2017-05-30 22:41:28', '2017-05-30 22:41:28', NULL, NULL),
(14, 'home-w.png', '6VuYuWDBPOotfBWvqWkZCQcOmCd-qdv0', '2017-05-31 21:35:13', '2017-05-31 21:35:13', NULL, NULL),
(15, '4cfd77054a9dbb73b6a2848f27a96e85.jpg', 'MciZ98vYkkAOO72Xz8VUnlNHkU3U6bCd', '2017-05-31 22:08:43', '2017-05-31 22:08:43', NULL, NULL),
(16, '5329e175e7f92.jpg', 'F5JeHFC7k98-6JvO9K3Yg6Ev7cBg968H', '2017-05-31 22:10:37', '2017-05-31 22:10:37', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1496161883),
('m130524_201442_init', 1496161887),
('m160622_085710_create_ImageManager_table', 1496163718),
('m170223_113221_addBlameableBehavior', 1496163721),
('m170530_153515_create_image_table', 1496161887),
('m170530_153856_create_comments_table', 1496161955),
('m170530_175136_create_album_table', 1496167010),
('m170530_175350_create_city_table', 1496167512),
('m170530_175626_create_portfolio_table', 1496167515),
('m170530_180313_add_album_column_to_city_table', 1496167518),
('m170530_210036_create_seo_table', 1496178076),
('m170531_045017_create_callback_table', 1496206584),
('m170531_161528_create_contacts_table', 1496247609),
('m170531_163205_add_hone_column_to_seo_table', 1496248344),
('m170531_170628_create_page_table', 1496250500),
('m170531_180311_create_slider_table', 1496253816);

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description_long` text,
  `description_page` text,
  `description_sales` text,
  `photo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`id`, `name`, `title`, `description_long`, `description_page`, `description_sales`, `photo`) VALUES
(1, 'СТРОИТЕЛЬСТВО ДОМОВ ПОД КЛЮЧ', 'СТРОИТЕЛЬСТВО ДОМОВ ПОД КЛЮЧ', '<p>Актуальная услуга для всех, кто хочет иметь собственный дом нужного типа, планировки и дизайна, без лишних трат и организационных проблем: строительство домов &laquo;под ключ&raquo; от компании &laquo;СтройФорс 57&raquo;!</p>\r\n\r\n<p>При покупке готового дома невозможно внести коррективы в типовой проект. При самостоятельной постройке и отделке вас ожидает масса затраченного времени, сил, денег и неизбежные для неспециалистов ошибки. Заказывая коттедж &laquo;под ключ&raquo;, вы получаете полностью готовое к вселению жилье со всеми коммуникациями, стопроцентно соответствующее как строительным нормам, так и вашим ожиданиям. Хотите видеть, как вырастает ваш дом? Мы организуем видеонаблюдение за строительством.</p>\r\n\r\n<p>НАША ОСНОВНАЯ ЦЕЛЬ - ПОСТРОИТЬ ВАМ ЗДАНИЕ В МАКСИМАЛЬНО КОМФОРТНЫХ ДЛЯ ВАС УСЛОВИЯХ. НАШ СЛОГАН: &laquo;СТРОГОЕ СОБЛЮДЕНИЕ ОБЯЗАТЕЛЬСТВ, ПРОВЕРЕННОЕ ВРЕМЕНЕМ КАЧЕСТВО&raquo;.</p>\r\n', '<p style="text-align: right;">ГОТОВЫЙ ДОМ</p>\r\n\r\n<h2 style="text-align: right;"><span style="font-size:26px"><span style="color:#FF8C00">ОТ 990 000 РУБЛЕЙ!</span></span></h2>\r\n\r\n<p style="text-align: right;">БЕСПЛАТНО ВЫПОЛНИМ ЗАМЕРЫ&nbsp;<br />\r\n<strong>И СДЕЛАЕМ<br />\r\nИНДИВИДУАЛЬНЫЙ ПРОЕКТ</strong><br />\r\nБУДУЩЕГО ДОМА!</p>\r\n', '<p>ОТ 990 000 Р ЗА ГОТОВЫЙ ДОМ</p>\r\n\r\n<p>ТОЧНУЮ ЦЕНУ МОЖНО УЗНАТЬ ПРИ ПРОСЧЕТЕ<br />\r\nНАШИМ СПЕЦИАЛИСТОМ ВСЕХ ВАШИХ ПОЖЕЛАНИЙ</p>\r\n', 14),
(2, 'ОТДЕЛОЧНЫЕ РАБОТЫ', NULL, NULL, NULL, NULL, NULL),
(3, 'МОНТАЖ ВЕНТИЛИРУЕМЫХ ФАСАДОВ', NULL, NULL, NULL, NULL, NULL),
(4, 'ПРОЕКТИРОВАНИЕ И ДИЗАЙН', NULL, NULL, NULL, NULL, NULL),
(5, 'АРХИТЕКТУРНОЕ ОСВЕЩЕНИЕ ФАСАДОВ И РЕКЛАМА', NULL, NULL, NULL, NULL, NULL),
(6, 'ИЗГОТОВЛЕНИЕ ФАСАДНЫХ КАССЕТ', NULL, NULL, NULL, NULL, NULL),
(7, 'Акция', 'Акция', '<p><span style="color:rgb(252, 182, 3); font-family:scada; font-size:3rem">ПРОЕКТ В ПОДАРОК</span><br />\r\n<strong><span style="color:rgb(255, 255, 255); font-family:scada; font-size:33.6px">ПРИ ЗАКЛЮЧЕНИИ ДОГОВОРА</span><br />\r\n<span style="color:rgb(255, 255, 255); font-family:scada; font-size:33.6px">НА СТРОИТЕЛЬСТВО КОТТЕДЖА!</span></strong></p>\r\n', '', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `portfolio`
--

CREATE TABLE `portfolio` (
  `id` int(11) NOT NULL,
  `city_id` int(11) DEFAULT NULL,
  `photo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `portfolio`
--

INSERT INTO `portfolio` (`id`, `city_id`, `photo`) VALUES
(1, 2, 13),
(2, 1, 11),
(3, 1, 9);

-- --------------------------------------------------------

--
-- Table structure for table `seo`
--

CREATE TABLE `seo` (
  `id` int(11) NOT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `h1` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `seo`
--

INSERT INTO `seo` (`id`, `alias`, `title`, `description`, `keywords`, `h1`) VALUES
(1, '/service/1', 'Строительство домов под ключ', 'Строительство домов под ключ дескрипшн', 'Строительство, дома', 'СТРОИТЕЛЬСТВО ДОМОВ ПОД КЛЮЧ'),
(2, '/service/2', 'Отделка домов и квартир под ключ', 'Отделка домов и квартир под ключ Дескрипшн', 'Отдел домов, под ключ', 'ОТДЕЛКА ДОМОВ И КВАРТИР ПОД КЛЮЧ'),
(3, '/portfolio', 'Портфолио', 'Портфолио дескрипшн', 'Портфолио, сф', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `title`, `photo`) VALUES
(1, 'Проектирование и дизайн', '15'),
(2, 'Строительство зданий "Под ключ"', '16');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'admin', '', '$2y$13$9PC3cVGm9P7/ImJdV/u21.fpwjiImQzUY3tNBr0DZ6VWPfD7srn1S', NULL, 'admin@admin.dev', 10, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `callback`
--
ALTER TABLE `callback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-city-album_id` (`album_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imagemanager`
--
ALTER TABLE `imagemanager`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `portfolio`
--
ALTER TABLE `portfolio`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-portfolio-city_id` (`city_id`);

--
-- Indexes for table `seo`
--
ALTER TABLE `seo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `album`
--
ALTER TABLE `album`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `callback`
--
ALTER TABLE `callback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `imagemanager`
--
ALTER TABLE `imagemanager`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `portfolio`
--
ALTER TABLE `portfolio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `seo`
--
ALTER TABLE `seo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `city`
--
ALTER TABLE `city`
  ADD CONSTRAINT `fk-city-album_id` FOREIGN KEY (`album_id`) REFERENCES `album` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `portfolio`
--
ALTER TABLE `portfolio`
  ADD CONSTRAINT `fk-portfolio-city_id` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
