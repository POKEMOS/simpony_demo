<?php

use yii\db\Migration;

/**
 * Handles the creation of table `seo`.
 */
class m170530_210036_create_seo_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('seo', [
            'id' => $this->primaryKey(),
            'alias' => $this->string(255),
            'title' => $this->string(255),
            'description' => $this->string(255),
            'keywords' => $this->string(255),
        ],'DEFAULT CHARSET=utf8');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('seo');
    }
}
