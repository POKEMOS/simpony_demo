<?php

use yii\db\Migration;

/**
 * Handles adding works to table `contacts`.
 */
class m170614_154004_add_works_column_to_contacts_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('contacts', 'year_number', $this->integer());
        $this->addColumn('contacts', 'work_number', $this->integer());
        $this->addColumn('contacts', 'clients_number', $this->integer());
        $this->addColumn('contacts', 'projects_number', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('contacts', 'year_number');
        $this->dropColumn('contacts', 'work_number');
        $this->dropColumn('contacts', 'clients_number');
        $this->dropColumn('contacts', 'projects_number');
    }
}
