<?if($works):?>
    <?
        $k = -1;
    ?>
<section class="sf_m_box_carusel_img job">
    <div class="middle">
        <h3>НАШИ РАБОТЫ</h3>
        <?/*?><span class="name_job">Строительство зданий под ключ</span><?*/?>
        <div class="owl-carousel owl-theme" id="sl_job">
            <?foreach($works as $key => $work):?>
                <?if($key%2 == 0):?>
                    <div class="item">
                <?endif;?>
                    <a rel="simplebox" class="a-work-job" href="<?=Yii::$app->imagemanager->getImagePath($work->img, 5000, 5000,'inset')?>">
                        <div class="carusel_hover_job work-block">
                            <strong><?=$work->title?></strong>
                            <?=$work->description?>
                        </div>
                        <img src="<?=Yii::$app->imagemanager->getImagePath($work->img, 5000, 5000,'inset')?>" alt="">
                    </a>
                <?if($key - $k == 2):?>
                    </div>
                    <?$k += 2?>
                <?endif;?>
            <?endforeach;?>
        </div>
    </div>
</section>
<?endif;?>