<?php

use yii\db\Migration;

/**
 * Handles adding video to table `comments`.
 */
class m170614_154259_add_video_column_to_comments_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('comments', 'video', $this->string(255));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('comments', 'video');
    }
}
