<?php

namespace frontend\assets;

use Yii;
use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'styles/screen.css',
        'styles/style.css',
        'owl-carousel/owl.carousel.css',
        'owl-carousel/owl.theme.css',
    ];
    public $js = [
        'js/jquery-3.1.1.min.js',
        'remodal/remodal.min.js',
        'owl-carousel/owl.carousel.js',
        'js/simplebox_util.js',
        'js/glisse.js',
        //'//api-maps.yandex.ru/2.1/?lang=ru_RU',
        'js/app.js',
        'js/custom.js'
    ];
    public $depends = [
        /*'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',*/
    ];

    public function __construct()
    {
        if(yii::$app->controller->id == 'site' && yii::$app->controller->action->id == 'portfolio') {
            $this->js[0] = 'http://code.jquery.com/jquery-1.7.1.min.js';
        }
    }
}
