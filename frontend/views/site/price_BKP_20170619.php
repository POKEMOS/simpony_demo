<section class="sf_top_text akk selle">
    <div class="middle">
        <p>Стоимость</p>
        <h1><strong>ГОТОВЫЙ ДОМ</strong> ОТ 999 000 Р</h1>

        <div class="m_b_cons_call right">
            <form action="/callback" class="flex_col" method="post">
                <div>
                    <input placeholder="Имя" name="name" type="text" required>
                </div>
                <div>
                    <input placeholder="Телефон" name="phone" type="text" required>
                </div>
                <div>
                    <input type="submit" value="ХОЧУ УЗНАТЬ ПОДРОБНЕЕ">
                </div>
            </form>
        </div>
        <div class="caruselki">
            <h3><?=$services[1]['title']?></h3>
            <div class="bn_other">
                <?=$services[1]['description_sales']?>
            </div>
            <button data-remodal-target="secondModalschet" type="submit">РАССЧИТАТЬ СТОИМОСТЬ</button>
            <h3><?=$services[2]['title']?></h3>
            <div class="owl-carousel owl-theme" id="sl_1_otdelka">
                <div class="item rem_box">
                    <img src="/img/sf_car_remont1.png" alt="">
                    <a href="#">Укладка плитки<br><br>
                        400Р/м2
                    </a>
                </div>
                <div class="item rem_box">
                    <img src="/img/sf_car_remont2.png" alt="">
                    <a href="#">Укладка шумо- <br>теплоизоляции <br>
                        400Р/м2
                    </a>
                </div>
                <div class="item rem_box">
                    <img src="/img/sf_car_remont3.png" alt="">
                    <a href="#">Укладка  ламината<br><br>400Р/м2</a>
                </div>
                <div class="item rem_box">
                    <img src="/img/sf_car_remont4.png" alt="">
                    <a href="#">штукатурка стен<br><br>400Р/м2</a>
                </div>
                <div class="item rem_box">
                    <img src="/img/sf_car_remont5.png" alt="">
                    <a href="#">Оклейка стен <br>обоями <br>220Р/м2 </a>
                </div>
                <div class="item rem_box">
                    <img src="/img/sf_car_remont6.png" alt="">
                    <a href="#">ОБЛИЦОВКА СТЕН<br> ДЕКОРАТИВНЫМ КАМНЕМ<br>850Р/м2</a>
                </div>
            </div>
            <div class="otdel">
                <button data-remodal-target="secondModalOtdelka" type="submit">Смотреть полный прайс Полы</button>
                <button data-remodal-target="secondModalOtdelkasten" type="submit">Смотреть полный прайс Стены</button>
            </div>
            <h3><?=$services[3]['title']?></h3>
            <div class="owl-carousel owl-theme" id="sl_price">
                <div class="item rem_box">
                    <img src="/img/sf_montaj_p1.png" alt="">
                    <a href="#">керамогранит<br><br>
                        от 2400Р / м2
                    </a>
                </div>
                <div class="item rem_box">
                    <img src="/img/sf_montaj_p2.png" alt="">
                    <a href="#">фиброцементный <br>сайдинг<br>
                        от 3500Р / м2
                    </a>
                </div>
                <div class="item rem_box">
                    <img src="/img/sf_montaj_p3.png" alt="">
                    <a href="#">дизайн<br> экстерьера<br>400Р/м2</a>
                </div>
                <div class="item rem_box">
                    <img src="/img/sf_montaj_p6.png" alt="">
                    <a href="#">Витражное<br> остекление<br>от 9000Р / м2</a>
                </div>
                <div class="item rem_box">
                    <img src="/img/sf_montaj_p4.png" alt="">
                    <a href="#">Алюминиевые <br>композитные панели<br>от 3000Р / м2</a>
                </div>
                <div class="item rem_box">
                    <img src="/img/sf_montaj_p5.png" alt="">
                    <a href="#">металлические <br>кассеты<br>от 2400Р / м2</a>
                </div>

            </div>
            <button data-remodal-target="secondModalm" type="submit">Смотреть полный прайс</button>
            <h3><?=$services[4]['title']?></h3>
            <div class="owl-carousel owl-theme" id="sl_2_price">
                <div class="item rem_box">
                    <img src="/img/sf_progect_box1.png" alt="">
                    <a href="#">Проектирование<br><br>
                        от 2000Р / м2
                    </a>
                </div>
                <div class="item rem_box">
                    <img src="/img/sf_progect_box2.png" alt="">
                    <a href="#">дизайн <br>интерьера<br>
                        от 500Р / м2
                    </a>
                </div>
                <div class="item rem_box">
                    <img src="/img/sf_progect_box3.png" alt="">
                    <a href="#">дизайн<br> экстерьера<br>400Р/м2</a>
                </div>

            </div>
            <button data-remodal-target="secondModalschet" type="submit">РАССЧИТАТЬ СТОИМОСТЬ</button>
            <h3><?=$services[5]['title']?></h3>
            <div class="bn_other arhi">
                <?=$services[5]['description_sales']?>
            </div>
            <button data-remodal-target="secondModalschet" type="submit">РАССЧИТАТЬ СТОИМОСТЬ</button>
            <h3>Наружная реклама</h3>
            <div class="bn_other arhi">
                <p class="h">ОТ 1000 Р</p>
                <p>
                    Точную цену можно УЗНАТЬ при просчете<br>нашим специалистом вашего тех. задания
                </p>
            </div>
            <button data-remodal-target="secondModalschet" type="submit">РАССЧИТАТЬ СТОИМОСТЬ</button>
            <H3><?=$services[6]['title']?></H3>
            <div class="flex_row">
                <div class="bn_other left">
                    <p class="h">без стоимости материала</p>
                    <p>
                        фрезеровка     &nbsp;&nbsp;&nbsp;&nbsp; 250р / м2 <br>гибка металокассет  &nbsp;&nbsp;  400р / м2
                    </p>
                </div>
                <div class="bn_other right">
                    <p class="h">с учетом стоимости материала</p>
                    <p>
                        Кассеты из АКП     &nbsp;&nbsp;&nbsp;&nbsp; 1250р / м2<br>кассеты из металла  &nbsp;&nbsp;&nbsp;  от 700р / м2
                    </p>
                </div>
            </div>
            <button data-remodal-target="secondModalschet" type="submit">РАССЧИТАТЬ СТОИМОСТЬ</button>
        </div>
    </div>
</section>