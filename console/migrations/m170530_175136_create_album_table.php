<?php

use yii\db\Migration;

/**
 * Handles the creation of table `album`.
 */
class m170530_175136_create_album_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('album', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'photo' => $this->integer(),
            'hidden' => $this->integer()->defaultValue(0),
        ],'DEFAULT CHARSET=utf8');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('album');
    }
}
