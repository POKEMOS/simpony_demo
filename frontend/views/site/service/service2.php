<section class="sf_top_text unic_1 remont">
    <div class="middle">
        <h1><?=$h1?></h1>
        <?=yii::$app->controller->renderPartial('//widgets/footer_callback')?>
        <div class="text_top-ramka">
            <div>
                <?=$description_long?>
            </div>
        </div>
    </div>
</section>
<section class="sf_m_box_utp remont">
    <div class="middle">
        <div class="line_home">
        </div>
        <img src="<?=Yii::$app->imagemanager->getImagePath($img, 5000, 5000,'inset')?>" alt="">
        <div class="sf_utp_text">
            <?=$description_page?>
			<button data-remodal-target="secondModal2">Оставить заявку</button>																		
        </div>
    </div>
</section>
<section class="sf_m_box_advantage">
    <div class="middle anim_plus">
        <h3>НАШИ ПРЕИМУЩЕСТВА</h3>
        <div class="flex_mid">
            <div ><img src="/img/sf_ad_box1.png" alt="pictures">
                <div>
                    <p class="big">НИЗКИЕ ЦЕНЫ</p>
                    <p>Мы гарантируем Вам <br>самую низкую цену</p>
                </div>
            </div>
            <div >
                <img src="/img/sf_ad_box3.png" alt="pictures">
                <div>
                    <p class="big">ОПЫТНЫЕ <br> СПЕЦИАЛИСТЫ</p>
                    <p>Сотрудники компании — <br>специалисты с <br>многолетним опытом</p>
                </div>
            </div>
        </div>
        <div class="flex_mid">
            <div >
                <img src="/img/sf_ad_box2.png" alt="pictures">
                <div>
                    <p class="big">ГАРАНТИЯ <br> КАЧЕСТВА</p>
                    <p>Мы предоставляем <br>Вам гарантию <br>на 5 лет</p>
                </div>
            </div>
            <div >
                <img src="/img/sf_ad_box4.png" alt="pictures">
                <div>
                    <p class="big">ЛЮБЫЕ <br>СПОСОБЫ ОПЛАТЫ</p>
                    <p>Оплачивайте наши услуги <br>любым доступным <br>Вам способом</p>
                </div>
            </div>
        </div>
    </div>
</section>

<?=yii::$app->controller->renderPartial('//widgets/works',['works' => $works])?>

<section class="m_box_advantage gar">
    <div class="middle anim_plus">
        <h3>ВАШИ ГАРАНТИИ</h3>
        <div class="flex_mid">
            <div><img src="/img/sf_gar_box1.png" alt="pictures">
                <p class="gar_text">Прозрачная<br>строительная<br>смета</p>
            </div>
            <div><img src="/img/sf_gar_box2.png" alt="pictures">
                <p class="gar_text">Поэтапная<br>сдача работ</p>
            </div>
            <div><img src="/img/sf_gar_box3.png" alt="pictures">
                <p class="gar_text">Точное выполнение<br>календарного<br>плана работ</p>
            </div>
            <div><img src="/img/sf_gar_box4.png" alt="pictures">
                <p class="gar_text">Онлайн-наблюдение<br>за строительством</p>
            </div>
        </div>
    </div>
</section>
<section class="sf_m_box_estimate">
    <div class="middle">
        <h2>Есть готовый проект дома?</h2>
        <span>Оценим и пришлем смету в течение 1 рабочего дня!</span>
        <button data-remodal-target="secondModalschet" type="submit">Рассчитать</button>
    </div>
</section>
<section class="sf_m_box_other remont">
    <div class="middle">
        <h3>стоимость отделки домов «под ключ»</h3>
        <div class="text_top-ramka">
            <div>
                <p>
                    Мы работаем по самой удобной для заказчиков схеме — от вас требуется только высказать свои пожелания и одобрить предложенный проект. В сферу нашей компетенции входит:
                </p>
                <div class="flex_row">
                    <ul class="list_other">
                        <li>закупка материалов</li>
                        <li>гидроизоляция пола</li>
                        <li>монтаж систем «теплый полов»</li>
                        <li>грунтовка</li>
                        <li>черновая и чистовая обшивка</li>
                        <li>облицовка, наклейка обоев</li>
                    </ul>
                    <ul class="list_other">
                        <li>устройство полов</li>
                        <li>укладка покрытий</li>
                        <li>укладка шумо- и теплоизоляции</li>
                        <li>штукатурка и шпатлевка стен</li>
                        <li>возведение межкомнатных перегородок</li>
                        <li>установка и подключение оборудования</li>
                    </ul>
                </div>
                <p>Точную стоимость услуги рассчитает наш специалист, исходя из объема и сложности поставленной задачи. Ремонт квартир и коттеджей, отделка домов «под ключ» не отнимет у вас много времени и финансов, если вы – клиент компании «СтройФорс 57»!
                </p>
            </div>
        </div>
        <div class="owl-carousel owl-theme" id="sl_price">
            <div class="item rem_box">
                <img src="/img/sf_car_remont1.png" alt="">
                <a href="#">Укладка плитки<br><br>
                    400Р/м2
                </a>
            </div>
            <div class="item rem_box">
                <img src="/img/sf_car_remont2.png" alt="">
                <a href="#">Укладка шумо- <br>теплоизоляции <br>
                    400Р/м2
                </a>
            </div>
            <div class="item rem_box">
                <img src="/img/sf_car_remont3.png" alt="">
                <a href="#">Укладка  ламината<br><br>400Р/м2</a>
            </div>
            <div class="item rem_box">
                <img src="/img/sf_car_remont4.png" alt="">
                <a href="#">штукатурка стен<br><br>400Р/м2</a>
            </div>
            <div class="item rem_box">
                <img src="/img/sf_car_remont5.png" alt="">
                <a href="#">Оклейка стен <br>обоями <br>220Р/м2 </a>
            </div>
            <div class="item rem_box">
                <img src="/img/sf_car_remont6.png" alt="">
                <a href="#">ОБЛИЦОВКА СТЕН<br> ДЕКОРАТИВНЫМ КАМНЕМ<br>850Р/м2</a>
            </div>
        </div>

        <div class="otdel">
            <button data-remodal-target="secondModalOtdelka" onclick="yaCounter44951161.reachGoal('View_Price')" type="submit">Полный прайс Полы Sale %</button>
            <button data-remodal-target="secondModalOtdelkasten" onclick="yaCounter44951161.reachGoal('View_Price')" type="submit">Полный прайс Стены Sale % </button>
        </div>
    </div>
</section>
<section class="sf_m_box_consultation">
    <div class="middle">
        <h2>ОСТАЛИСЬ ВОПРОСЫ?
            <span>Мы с радостью на них ответим. Заполните, пожалуйста, форму!</span>
        </h2>
        <?=yii::$app->controller->renderPartial('//widgets/callback_index')?>
    </div>
</section>
<section class="sf_m_box_outline">
    <div class="middle">
        <h3>СХЕМА РАБОТЫ</h3>
        <div class="flex_mid">
            <div>

                <img src="/img/sf_sh_box1.png" alt="">
                <p>Отправка<br>заявки</p>
                <p>Вы отправляете<br>заявку на сайте</p>
            </div>
            <div>

                <img src="/img/sf_sh_box2.png" alt="">
                <p>Коммерческое <br>предложение </p>
                <p>Мы <br>перезваниваем Вам<br>и обсуждаем проект</p>
            </div>
            <div>
                <img src="/img/sf_sh_box3.png" alt="">
                <p>Утверждение<br>сметы</p>
                <p>Подготавливаем<br>проект<br>и отправляем смету</p>
            </div>
            <div>
                <img src="/img/sf_sh_box4.png" alt="">
                <p>Договор</p>
                <p>Подписываем<br>договор<br>и производим<br>строительные работы</p>
            </div>
            <div>
                <img src="/img/sf_sh_box5.png" alt="">
                <p>Сдача проекта</p>
                <p>Мы точно в срок<br>сдаем Вам<br>качественную работу</p>
            </div>
        </div>
    </div>
</section>

<section class="sf_m_box_carusel_img sert">
    <div class="middle">
        <h3>СЕРТИФИКАТЫ</h3>
        <div class="owl-carousel owl-theme" id="sl_dock">
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat.jpg">
                    <img src="/img/sertificat.jpg" alt="">
                    <a href="#">Свидетельство</a></a>
            </div>
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat2.jpg">
                    <img src="/img/sertificat2.jpg" alt="">
                    <a href="#">Приложение 1</a></a>
            </div>
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat3.jpg">
                    <img src="/img/sertificat3.jpg" alt="">
                    <a href="#">Приложение 2</a></a>
            </div>
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat4.jpg">
                    <img src="/img/sertificat4.jpg" alt="">
                    <a href="#">Приложение 3</a>
                </a>
            </div>
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat5.jpg">
                    <img src="/img/sertificat5.jpg" alt="">
                    <a href="#">Приложение 4</a>
                </a>
            </div>
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat6.jpg">
                    <img src="/img/sertificat6.jpg" alt="">
                    <a href="#">Приложение 5</a>
                </a>
            </div>
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat7.jpg">
                    <img src="/img/sertificat7.jpg" alt="">
                    <a href="#">Приложение 6</a>
                </a>
            </div>
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat8.jpg">
                    <img src="/img/sertificat8.jpg" alt="">
                    <a href="#">Приложение 7</a>
                </a>
            </div>
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat9.jpg">
                    <img src="/img/sertificat9.jpg" alt="">
                    <a href="#">Приложение 8</a>
                </a>
            </div>
        </div>
    </div>
</section>
<?=$comments?>