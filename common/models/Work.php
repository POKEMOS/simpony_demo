<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "work".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $img
 * @property integer $service_id
 *
 * @property Page $service
 */
class Work extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'work';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['img', 'service_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['service_id'], 'exist', 'skipOnError' => true, 'targetClass' => Page::className(), 'targetAttribute' => ['service_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'img' => 'Img',
            'service_id' => 'Service',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Page::className(), ['id' => 'service_id']);
    }
}
