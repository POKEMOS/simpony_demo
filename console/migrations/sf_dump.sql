-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 01, 2017 at 09:24 
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sf`
--
--
-- Dumping data for table `page`
--

INSERT INTO `page` (`id`, `name`, `title`, `description_long`, `description_page`, `description_sales`, `photo`) VALUES
(1, 'СТРОИТЕЛЬСТВО ДОМОВ ПОД КЛЮЧ', 'СТРОИТЕЛЬСТВО ДОМОВ ПОД КЛЮЧ', '<p>Актуальная услуга для всех, кто хочет иметь собственный дом нужного типа, планировки и дизайна, без лишних трат и организационных проблем: строительство домов &laquo;под ключ&raquo; от компании &laquo;СтройФорс 57&raquo;!</p>\r\n\r\n<p>При покупке готового дома невозможно внести коррективы в типовой проект. При самостоятельной постройке и отделке вас ожидает масса затраченного времени, сил, денег и неизбежные для неспециалистов ошибки. Заказывая коттедж &laquo;под ключ&raquo;, вы получаете полностью готовое к вселению жилье со всеми коммуникациями, стопроцентно соответствующее как строительным нормам, так и вашим ожиданиям. Хотите видеть, как вырастает ваш дом? Мы организуем видеонаблюдение за строительством.</p>\r\n\r\n<p>НАША ОСНОВНАЯ ЦЕЛЬ - ПОСТРОИТЬ ВАМ ЗДАНИЕ В МАКСИМАЛЬНО КОМФОРТНЫХ ДЛЯ ВАС УСЛОВИЯХ. НАШ СЛОГАН: &laquo;СТРОГОЕ СОБЛЮДЕНИЕ ОБЯЗАТЕЛЬСТВ, ПРОВЕРЕННОЕ ВРЕМЕНЕМ КАЧЕСТВО&raquo;.</p>\r\n', '<p style="text-align: right;">ГОТОВЫЙ ДОМ</p>\r\n\r\n<h2 style="text-align: right;"><span style="font-size:26px"><span style="color:#FF8C00">ОТ 990 000 РУБЛЕЙ!</span></span></h2>\r\n\r\n<p style="text-align: right;">БЕСПЛАТНО ВЫПОЛНИМ ЗАМЕРЫ&nbsp;<br />\r\n<strong>И СДЕЛАЕМ<br />\r\nИНДИВИДУАЛЬНЫЙ ПРОЕКТ</strong><br />\r\nБУДУЩЕГО ДОМА!</p>\r\n', '<p>ОТ 990 000 Р ЗА ГОТОВЫЙ ДОМ</p>\r\n\r\n<p>ТОЧНУЮ ЦЕНУ МОЖНО УЗНАТЬ ПРИ ПРОСЧЕТЕ<br />\r\nНАШИМ СПЕЦИАЛИСТОМ ВСЕХ ВАШИХ ПОЖЕЛАНИЙ</p>\r\n', 14),
(2, 'ОТДЕЛОЧНЫЕ РАБОТЫ', NULL, NULL, NULL, NULL, NULL),
(3, 'МОНТАЖ ВЕНТИЛИРУЕМЫХ ФАСАДОВ', NULL, NULL, NULL, NULL, NULL),
(4, 'ПРОЕКТИРОВАНИЕ И ДИЗАЙН', NULL, NULL, NULL, NULL, NULL),
(5, 'АРХИТЕКТУРНОЕ ОСВЕЩЕНИЕ ФАСАДОВ И РЕКЛАМА', NULL, NULL, NULL, NULL, NULL),
(6, 'ИЗГОТОВЛЕНИЕ ФАСАДНЫХ КАССЕТ', NULL, NULL, NULL, NULL, NULL),
(7, 'Акция', 'Акция', '<p><span style="color:rgb(252, 182, 3); font-family:scada; font-size:3rem">ПРОЕКТ В ПОДАРОК</span><br />\r\n<strong><span style="color:rgb(255, 255, 255); font-family:scada; font-size:33.6px">ПРИ ЗАКЛЮЧЕНИИ ДОГОВОРА</span><br />\r\n<span style="color:rgb(255, 255, 255); font-family:scada; font-size:33.6px">НА СТРОИТЕЛЬСТВО КОТТЕДЖА!</span></strong></p>\r\n', '', '', NULL);

--
-- Dumping data for table `portfolio`
--

INSERT INTO `portfolio` (`id`, `city_id`, `photo`) VALUES
(1, 2, 13),
(2, 1, 11),
(3, 1, 9);

--
-- Dumping data for table `seo`
--

INSERT INTO `seo` (`id`, `alias`, `title`, `description`, `keywords`, `h1`) VALUES
(1, '/service/1', 'Строительство домов под ключ', 'Строительство домов под ключ дескрипшн', 'Строительство, дома', 'СТРОИТЕЛЬСТВО ДОМОВ ПОД КЛЮЧ'),
(2, '/service/2', 'Отделка домов и квартир под ключ', 'Отделка домов и квартир под ключ Дескрипшн', 'Отдел домов, под ключ', 'ОТДЕЛКА ДОМОВ И КВАРТИР ПОД КЛЮЧ'),
(3, '/portfolio', 'Портфолио', 'Портфолио дескрипшн', 'Портфолио, сф', NULL);

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `title`, `photo`) VALUES
(1, 'Проектирование и дизайн', '15'),
(2, 'Строительство зданий "Под ключ"', '16');

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'admin', '', '$2y$13$9PC3cVGm9P7/ImJdV/u21.fpwjiImQzUY3tNBr0DZ6VWPfD7srn1S', NULL, 'admin@admin.dev', 10, 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
