<section class="m_box_usel">

    <div class="m_sec_carusel">
        <div class="owl-carousel owl-theme" id="sl_one">
            <?foreach($slider as $item):?>
                <div class="item" style="background-image: url('<?=Yii::$app->imagemanager->getImagePath($item->photo, 5000, 5000,'inset')?>')">
                    <div class="sl_info-block">
                        <p>
                            <a href="javascript:void(0)"><?=$item->title?></a>
                        </p>
                        <button type="submit"  data-remodal-target="secondModal2">
                            Оставить заявку
                        </button>
                    </div>
                </div>
            <?endforeach?>
        </div>
    </div>
</section>
<a id="pro" style="position: absolute;top: 36rem;"></a>
<?=yii::$app->controller->renderPartial('//widgets/on_work');?>
<section class="sf_m_box_advantage">
    <div class="middle anim_plus">
        <h3>НАШИ ПРЕИМУЩЕСТВА</h3>
        <div class="flex_mid">
            <div ><img src="/img/sf_ad_box1.png" alt="pictures">
                <div>
                    <p class="big">НИЗКИЕ ЦЕНЫ</p>
                    <p>Мы гарантируем Вам <br>самую низкую цену</p>
                </div>
            </div>
            <div >
                <img src="/img/sf_ad_box3.png" alt="pictures">
                <div>
                    <p class="big">ОПЫТНЫЕ <br> СПЕЦИАЛИСТЫ</p>
                    <p>Сотрудники компании — <br>специалисты с <br>многолетним опытом</p>
                </div>
            </div>
        </div>
        <div class="flex_mid">
            <div >
                <img src="/img/sf_ad_box2.png" alt="pictures">
                <div>
                    <p class="big">ГАРАНТИЯ <br> КАЧЕСТВА</p>
                    <p>Мы предоставляем <br>Вам гарантию <br>на 5 лет</p>
                </div>
            </div>
            <div >
                <img src="/img/sf_ad_box4.png" alt="pictures">
                <div>
                    <p class="big">ЛЮБЫЕ <br>СПОСОБЫ ОПЛАТЫ</p>
                    <p>Оплачивайте наши услуги <br>любым доступным <br>Вам способом</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="sf_m_box_advantage uslugi">
    <div class="middle">
        <h3>НАШИ УСЛУГИ</h3>
        <div class="flex_mid">
            <a href="/service/1">
                <img src="/img/sf_us_box1_line1.png" alt="pictures">
                <p class="gar_text">Строительство<br>домов<br>под ключ</p>

            </a>
            <a href="/service/2">
                <img src="/img/sf_us_box2_line1.png" alt="pictures">
                <p class="gar_text">ОТДЕЛОЧНЫЕ <br>РАБОТЫ</p>

            </a>
        </div>
        <div class="flex_mid">
            <a href="/service/9">
                <img src="/img/sf_us_box1_line2.png" alt="pictures">
                <p class="gar_text">Промышленные<br>и наливные полы</p>

            </a>
            <a href="/service/8">
                <img src="/img/sf_us_box2_line2.png" alt="pictures">
                <p class="gar_text">кровельные<br>работы</p>

            </a>
            <a href="/service/6">
                <img src="/img/sf_us_box3_line2.png" alt="pictures">
                <p class="gar_text">ИЗГОТОВЛЕНИЕ<br>ФАСАДНЫХ<br>КАССЕТ</p>

            </a>
            <a href="/service/5">
                <img src="/img/sf_us_box4_line2.png" alt="pictures">
                <p class="gar_text">АРХИТЕКТУРНОЕ<br>ОСВЕЩЕНИЕ<br>и реклама</p>

            </a>
        </div>
        <div class="flex_mid">
            <a href="/service/4">
                <img src="/img/sf_us_box1_line3.png" alt="pictures">
                <p class="gar_text">проектирование<br>и дизайн</p>
            </a>
            <a href="/service/3">
                <img src="/img/sf_us_box2_line3.png" alt="pictures">
                <p class="gar_text">монтаж<br>вентилируемых<br>фасадов</p>
            </a>
        </div>
    </div>
    </div>
</section>
<section class="m_box_advantage gar">
    <div class="middle anim_plus">
        <h3>ВАШИ ГАРАНТИИ</h3>
        <div class="flex_mid">
            <div><img src="/img/sf_gar_box1.png" alt="pictures">
                <p class="gar_text">Прозрачная<br>строительная<br>смета</p>
            </div>
            <div><img src="/img/sf_gar_box2.png" alt="pictures">
                <p class="gar_text">Поэтапная<br>сдача работ</p>
            </div>
            <div><img src="/img/sf_gar_box3.png" alt="pictures">
                <p class="gar_text">Точное выполнение<br>календарного<br>плана работ</p>
            </div>
            <div><img src="/img/sf_gar_box4.png" alt="pictures">
                <p class="gar_text">Онлайн-наблюдение<br>за строительством</p>
            </div>
        </div>
    </div>
</section>
<?=yii::$app->controller->renderPartial('//widgets/works',['works' => $works])?>


<section class="sf_m_box_consultation">
    <div class="middle">
        <h2>ОСТАЛИСЬ ВОПРОСЫ?
            <span>Мы с радостью на них ответим. Заполните, пожалуйста, форму!</span>
        </h2>
       <?=yii::$app->controller->renderPartial('//widgets/callback_index')?>
    </div>
</section>
<section class="sf_m_box_outline">
    <div class="middle">
        <h3>СХЕМА РАБОТЫ</h3>
        <div class="flex_mid">
            <div>

                <img src="/img/sf_sh_box1.png" alt="">
                <p>Отправка<br>заявки</p>
                <p>Вы отправляете<br>заявку на сайте</p>
            </div>
            <div>

                <img src="/img/sf_sh_box2.png" alt="">
                <p>Коммерческое <br>предложение </p>
                <p>Мы <br>перезваниваем Вам<br>и обсуждаем проект</p>
            </div>
            <div>
                <img src="/img/sf_sh_box3.png" alt="">
                <p>Утверждение<br>сметы</p>
                <p>Подготавливаем<br>проект<br>и отправляем смету</p>
            </div>
            <div>
                <img src="/img/sf_sh_box4.png" alt="">
                <p>Договор</p>
                <p>Подписываем<br>договор<br>и производим<br>строительные работы</p>
            </div>
            <div>
                <img src="/img/sf_sh_box5.png" alt="">
                <p>Сдача проекта</p>
                <p>Мы точно в срок<br>сдаем Вам<br>качественную работу</p>
            </div>
        </div>
    </div>
</section>
<section class="sf_m_box_carusel_img sert">
    <div class="middle">
        <h3>СЕРТИФИКАТЫ</h3>
        <div class="owl-carousel owl-theme" id="sl_dock">
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat.jpg">
                    <img src="/img/sertificat.jpg" alt="">
                    <a href="#">Свидетельство</a></a>
            </div>
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat2.jpg">
                    <img src="/img/sertificat2.jpg" alt="">
                    <a href="#">Приложение 1</a></a>
            </div>
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat3.jpg">
                    <img src="/img/sertificat3.jpg" alt="">
                    <a href="#">Приложение 2</a></a>
            </div>
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat4.jpg">
                    <img src="/img/sertificat4.jpg" alt="">
                    <a href="#">Приложение 3</a>
                </a>
            </div>
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat5.jpg">
                    <img src="/img/sertificat5.jpg" alt="">
                    <a href="#">Приложение 4</a>
                </a>
            </div>
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat6.jpg">
                    <img src="/img/sertificat6.jpg" alt="">
                    <a href="#">Приложение 5</a>
                </a>
            </div>
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat7.jpg">
                    <img src="/img/sertificat7.jpg" alt="">
                    <a href="#">Приложение 6</a>
                </a>
            </div>
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat8.jpg">
                    <img src="/img/sertificat8.jpg" alt="">
                    <a href="#">Приложение 7</a>
                </a>
            </div>
            <div class="item sert">
                <a rel="simplebox" href="/img/sertificat9.jpg">
                    <img src="/img/sertificat9.jpg" alt="">
                    <a href="#">Приложение 8</a>
                </a>
            </div>
        </div>
    </div>
</section>
<a id="otziv"></a>
<?=$comments?>